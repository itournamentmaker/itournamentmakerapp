package com.littlebigpeople.itournamentmaker.tournament

import com.littlebigpeople.itournamentmaker.data.Discipline
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.data.Tournament
import com.littlebigpeople.itournamentmaker.data.TournamentDetails


interface TournamentGenerator{
    fun generateTournament(userId: String, tournamentId: String,playerCount:Int, teams:MutableList<Team>, disciplines:MutableList<Discipline>, tournamentDetails: TournamentDetails): Tournament


}
