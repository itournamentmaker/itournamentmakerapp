package com.littlebigpeople.itournamentmaker.tournament

import com.littlebigpeople.itournamentmaker.data.*
import java.util.*


class AllVsAll : TournamentGenerator {
    override fun generateTournament(userId: String, tournamentId: String,playerCount:Int, teams: MutableList<Team>, disciplines: MutableList<Discipline>, tournamentDetails: TournamentDetails): Tournament {


        val matches: ArrayList<Match> = ArrayList()
        // Inefficient. Quick dirty solution
        for (discipline in disciplines){
            val scheduled = HashSet<Team>()
            for (teamA in teams){
                scheduled.add(teamA)
                val minusTeamA = teams.minus(teamA)
                for (teamB in minusTeamA){
                    if (!scheduled.contains(teamB)) {
                        val match = Match(MatchState.READY,UUID.randomUUID().toString(),teamA, teamB, 0, 0, discipline)
                        matches.add(match)
                    }
                }
            }
        }
        return Tournament(tournamentDetails.name,tournamentId, userId,tournamentDetails.user,playerCount, teams)
    }


    fun generateSchedule(teams: MutableList<Team>, disciplines: MutableList<Discipline>): MutableList<Match>{
        val matches: ArrayList<Match> = ArrayList()
        // Inefficient. Quick dirty solution
        for (discipline in disciplines){
            val scheduled = HashSet<Team>()
            for (teamA in teams){
                scheduled.add(teamA)
                val minusTeamA = teams.minus(teamA)
                for (teamB in minusTeamA){
                    if (!scheduled.contains(teamB)) {
                        val match = Match(MatchState.READY,UUID.randomUUID().toString(),teamA, teamB, 0, 0, discipline)
                        matches.add(match)
                    }
                }
            }
        }
        return matches
    }



}