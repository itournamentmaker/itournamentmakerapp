package com.littlebigpeople.itournamentmaker.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.data.Discipline
import com.littlebigpeople.itournamentmaker.viewholders.DisciplineViewHolder
import kotlinx.android.synthetic.main.itemview_my_discipline.view.*

class MyDisciplineAdapter(private val dataset: ArrayList<Discipline>) : RecyclerView.Adapter<DisciplineViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DisciplineViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.itemview_my_discipline, parent, false)
        return DisciplineViewHolder(itemView)
    }

    override fun getItemCount() = dataset.size


    override fun onBindViewHolder(holder: DisciplineViewHolder, position: Int) {
        holder.itemView.list_discipline_name.text = dataset[position].name
        holder.itemView.list_discipline_description.text = dataset[position].description

    }
}