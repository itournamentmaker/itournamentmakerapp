package com.littlebigpeople.itournamentmaker.adapters

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.data.Discipline
import com.littlebigpeople.itournamentmaker.fragments.AddEditDisciplineFragment
import com.littlebigpeople.itournamentmaker.viewholders.DisciplineViewHolder
import kotlinx.android.synthetic.main.itemview_discipline.view.*

/**
 * Created by dennisschmock on 11/12/2017.
 */
class DisciplinesAdapter(val options: FirestoreRecyclerOptions<Discipline>, val context: Fragment, val tournamentId: String) : FirestoreRecyclerAdapter<Discipline, DisciplineViewHolder>(options) {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DisciplineViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.itemview_discipline, parent, false)
        return DisciplineViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: DisciplineViewHolder?, position: Int, discipline: Discipline) {
        if (holder != null) {
            holder.itemView.list_discipline_name.text = discipline.name
            holder.itemView.list_discipline_description.text = discipline.description
            holder.itemView.btn_edit_discipline.setOnClickListener {


                val fragment = AddEditDisciplineFragment()
                val bundle = Bundle()
                bundle.putString("disciplineid",discipline.uuid)
                bundle.putString("id", tournamentId)
                fragment.arguments = bundle
                context.fragmentManager.beginTransaction().replace(R.id.flContent,fragment).addToBackStack("listdisc").commit()

            }
        }



    }
}