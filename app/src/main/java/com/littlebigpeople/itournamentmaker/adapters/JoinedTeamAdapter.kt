package com.littlebigpeople.itournamentmaker.adapters

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.fragments.ListJoinedPlayersFragment
import com.littlebigpeople.itournamentmaker.state.TEAM_ID
import com.littlebigpeople.itournamentmaker.viewholders.TeamViewHolder
import kotlinx.android.synthetic.main.itemview_team.view.*

/**
 * Created by dennisschmock on 01/12/2017.
 */
class JoinedTeamAdapter(options: FirestoreRecyclerOptions<Team>, val context: Fragment) : FirestoreRecyclerAdapter<Team, TeamViewHolder>(options) {


    override fun onBindViewHolder(holder: TeamViewHolder, position: Int, team: Team) {

        if (holder!=null){
            holder.itemView.list_team_name.text = team.name

        }

        holder.itemView.btn_view_team.setOnClickListener {
            val bundle = Bundle()
            bundle.putString(TEAM_ID, team.tournamentId)

            val listPlayersFragment = ListJoinedPlayersFragment()
            listPlayersFragment.arguments = bundle

            context.fragmentManager.beginTransaction().replace(R.id.flContent,listPlayersFragment).addToBackStack("TeamsAdapter").commit()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder{
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.itemview_team, parent, false)


        return TeamViewHolder(itemView)
    }
}