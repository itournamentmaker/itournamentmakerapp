package com.littlebigpeople.itournamentmaker.adapters

import android.annotation.SuppressLint
import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.data.Match
import com.littlebigpeople.itournamentmaker.data.MatchState
import com.littlebigpeople.itournamentmaker.fragments.UpdateMatchFragment
import com.littlebigpeople.itournamentmaker.state.MATCH_ID
import com.littlebigpeople.itournamentmaker.state.TOURNAMENT_ID
import com.littlebigpeople.itournamentmaker.viewholders.MatchViewHolder

import kotlinx.android.synthetic.main.itemview_match.view.*

/**
 * Created by dennisschmock on 11/12/2017.
 */
class MatchesAdapter(val options: FirestoreRecyclerOptions<Match>, val context: Fragment, val tournamentId: String) : FirestoreRecyclerAdapter<Match, MatchViewHolder>(options) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.itemview_match, parent, false)
        return MatchViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MatchViewHolder?, position: Int, match: Match) {

        if (holder != null) {
            val matchStatus: String = if(match.state == MatchState.PLAYED) {
                "Played!"
            }else{
                "Ready!"
            }

            if(match.state == MatchState.PLAYED){
                holder.itemView.btn_edit_match.visibility = View.INVISIBLE
            }


            holder.itemView.list_match_discipline_name.text = match.discipline.name
            holder.itemView.match_team_a_name.text = match.teamA.name + context.getString(R.string.score_text) + match.scoreA
            holder.itemView.match_team_b_name.text = match.teamB.name + context.getString(R.string.score_text) + match.scoreB
            holder.itemView.match_status.text = matchStatus

            holder.itemView.btn_edit_match.setOnClickListener {
                val fragment = UpdateMatchFragment()
                val bundle = Bundle()
                bundle.putString(MATCH_ID,match.matchId)
                bundle.putString(TOURNAMENT_ID, tournamentId)
                fragment.arguments = bundle
                context.fragmentManager.beginTransaction().replace(R.id.flContent,fragment).addToBackStack("listdisc").commit()
            }
        }

    }
}