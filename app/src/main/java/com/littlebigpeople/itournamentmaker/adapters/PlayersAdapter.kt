package com.littlebigpeople.itournamentmaker.adapters

import android.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.data.Player
import com.littlebigpeople.itournamentmaker.viewholders.PlayerViewHolder
import kotlinx.android.synthetic.main.itemview_player.view.*

/**
 * Created by dennisschmock on 01/12/2017.
 */
class PlayersAdapter(options: FirestoreRecyclerOptions<Player>, val context: Fragment) : FirestoreRecyclerAdapter<Player, PlayerViewHolder>(options) {


    override fun onBindViewHolder(holder: PlayerViewHolder, position: Int, player: Player) {

        if (holder!=null){
            holder.itemView.list_player_name.text = player.name

            holder.itemView.list_player_team.text = player.teamName

            holder.itemView.btn_view_player.setOnClickListener {
            }
        }


    }

    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerViewHolder{

        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.itemview_player, parent, false)


        return PlayerViewHolder(itemView)
    }
}