package com.littlebigpeople.itournamentmaker.adapters

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.data.Tournament
import com.littlebigpeople.itournamentmaker.fragments.TournamentDashboardFragment
import com.littlebigpeople.itournamentmaker.viewholders.TournamentViewHolder
import kotlinx.android.synthetic.main.itemview_tournament.view.*


/**
 * Created by dennisschmock on 01/12/2017.
 */

open class TournamentsAdapter(val options: FirestoreRecyclerOptions<Tournament>, val context: Fragment) : FirestoreRecyclerAdapter<Tournament, TournamentViewHolder>(options) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TournamentViewHolder {


        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.itemview_tournament, parent, false)

        return TournamentViewHolder(itemView)

    }




    override fun onDataChanged() {
        super.onDataChanged()
        (context.activity as MainActivity).hideSpinner()

    }

    override fun onBindViewHolder(holder: TournamentViewHolder?, position: Int, tournament: Tournament?) {

        if (holder != null && tournament != null) {

            holder.title.text = tournament.name
            holder.description.text = tournament.username

            holder.itemView.btn_tournament.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("id", tournament.tournamentId)

                val tournamentsFragment = TournamentDashboardFragment()
                tournamentsFragment.arguments = bundle

              this.context.fragmentManager.beginTransaction().replace(R.id.flContent,tournamentsFragment).addToBackStack(null).commit()

            }


        }


    }
}