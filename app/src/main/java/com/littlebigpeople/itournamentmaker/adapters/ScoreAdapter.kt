package com.littlebigpeople.itournamentmaker.adapters

import android.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.viewholders.ScoreViewHolder
import kotlinx.android.synthetic.main.itemview_score.view.*

/**
 * Created by Dennis on 02-01-2018.
 */
class ScoreAdapter(options: FirestoreRecyclerOptions<Team>, val context: Fragment) : FirestoreRecyclerAdapter<Team, ScoreViewHolder>(options) {


    override fun onBindViewHolder(holder: ScoreViewHolder, position: Int, team: Team) {

        if (holder!=null){
            holder.itemView.list_score_team_name.text = team.name
            holder.itemView.text_score.text = team.points.toString()

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScoreViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.itemview_score, parent, false)


        return ScoreViewHolder(itemView)
    }
}