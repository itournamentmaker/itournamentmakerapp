package com.littlebigpeople.itournamentmaker.adapters.controllers

import android.content.Context
import android.graphics.*
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


abstract class SwipeHelper(val context: Context, val recyclerView: RecyclerView) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {


    val buttonWidth: Int = 100
    private var buttons: ArrayList<UnderlayButton> = ArrayList()
    private var swipedPos = -1
    private var swipeThreshold = 0.5f
    private var buttonsBuffer: Map<Int, ArrayList<UnderlayButton>> = HashMap()
    private var recoverQueue: Queue<Int> = object : LinkedList<Int>() {
        override fun add(o: Int): Boolean {
            return if (contains(o)) {
                false
            } else {
                super.add(o)
            }

        }

    }




    private val gestureListener = object : GestureDetector.SimpleOnGestureListener() {

        override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
            Log.d("click action", "no")
            for (button in buttons) {
                if (e != null) {
                    if (button.onClick(e.getX(), e.getY())) {
                        break
                    }
                }

            }
            return true
        }
    }

    private val gestureDetector: GestureDetector = GestureDetector(context, gestureListener)



    private val onTouchListener = object : View.OnTouchListener {
        override fun onTouch(view: View, e: MotionEvent): Boolean {
            if(swipedPos<0) return false

            val point = Point(e.rawX.toInt(),e.rawY.toInt())
            val swipedViewHolder = recyclerView.findViewHolderForAdapterPosition(swipedPos)
            val swipedItem = swipedViewHolder.itemView
            val rect = Rect()
            swipedItem.getGlobalVisibleRect(rect)
            if(e.action==MotionEvent.ACTION_DOWN||e.action==MotionEvent.ACTION_UP||e.action==MotionEvent.ACTION_MOVE){

                if(rect.top<point.y&&rect.bottom>point.y){
                    gestureDetector.onTouchEvent(e)
                }else{
                    recoverQueue.add(swipedPos)
                    swipedPos = -1
                    recoverSwipedItem()
                }
            }
            return false
        }

    }


    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder?): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val pos = viewHolder.adapterPosition
        if (swipedPos != pos) recoverQueue.add(swipedPos)

        swipedPos = pos

        if (buttonsBuffer.containsKey(swipedPos)) {
            buttons = buttonsBuffer[swipedPos]!!
        } else buttons.clear()

        (buttonsBuffer as HashMap).clear()
        swipeThreshold = 0.5f * buttons.size * buttonWidth
        recoverSwipedItem()


    }

    override fun getSwipeThreshold(viewHolder: RecyclerView.ViewHolder?): Float {
        return swipeThreshold
    }

    override fun getSwipeEscapeVelocity(defaultValue: Float): Float {
        return 0.1f * defaultValue
    }

    override fun getSwipeVelocityThreshold(defaultValue: Float): Float {
        return 5.0f * defaultValue
    }


    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        val pos = viewHolder.adapterPosition
        var translationX = dX
        val itemView = viewHolder.itemView
        if (pos < 0) {
            swipedPos = pos
            return
        }

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            if (dX < 0) {
                var buffer = arrayListOf<UnderlayButton>()
                if (!buttonsBuffer.containsKey(pos)) {
                    instantiateUnderlayButton(viewHolder, buffer)
                    (buttonsBuffer as HashMap)[pos] = buffer
                } else {
                    buffer = buttonsBuffer[pos] as ArrayList
                }
                translationX = dX * buffer.size * buttonWidth / itemView.width


                drawButtons(c, itemView, buffer, pos, translationX)
            }
        }

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    @Synchronized
    private fun recoverSwipedItem() {
        while (!recoverQueue.isEmpty()) {
            val pos = recoverQueue.poll()
            if (pos > -1) {
                recyclerView.adapter.notifyItemChanged(pos)
            }
        }
    }

    private fun drawButtons(c: Canvas, itemView: View, buffer: List<UnderlayButton>, pos: Int, dX: Float) {

        var right: Float = itemView.right.toFloat()
        val dButtonWidth = (-1) * dX / buffer.size
        for (button in buffer) {
            val left: Float = right - dButtonWidth
            button.onDraw(c, RectF(left, itemView.top.toFloat(), right, itemView.bottom.toFloat()), pos)
            right = left
        }

    }

    fun attachSwipe() {
        val itemTouchHelper = ItemTouchHelper(this)
        itemTouchHelper.attachToRecyclerView(recyclerView)

    }


    abstract fun instantiateUnderlayButton(viewHolder: RecyclerView.ViewHolder, underlayButtons: List<UnderlayButton>)

    class UnderlayButton(private val text: String, private val imageResId: Int, private val color: Int, private val clickListener: UnderlayButtonClickListener) {
        private var pos: Int = 0
        private var clickRegion: RectF? = null

        fun onClick(x: Float, y: Float): Boolean {
            if (clickRegion != null && clickRegion!!.contains(x, y)) {
                clickListener.onClick(pos)
                Log.d("click ", "no")

                return true
            }
            Log.d("click ", "yes")

            return false
        }

        fun onDraw(c: Canvas, rect: RectF, pos: Int) {
            val p = Paint()

            // Draw background
            p.color = color
            c.drawRect(rect, p)

            // Draw Text
            p.color = Color.WHITE
            p.textSize = 60f

            val r = Rect()
            val cHeight = rect.height()
            val cWidth = rect.width()
            p.textAlign = Paint.Align.LEFT
            p.getTextBounds(text, 0, text.length, r)
            val x = cWidth / 2f - r.width() / 2f - r.left
            val y = cHeight / 2f + r.height() / 2f - r.bottom
            c.drawText(text, rect.left + x, rect.top + y, p)

            clickRegion = rect
            this.pos = pos
        }
    }

    interface UnderlayButtonClickListener {
        fun onClick(pos: Int)
    }

    init {
        attachSwipe()
        recyclerView.setOnTouchListener(onTouchListener)
    }
}

