package com.littlebigpeople.itournamentmaker.adapters.controllers

abstract open class SwipeControllerActions {
    fun onEditClicked(position: Int) {}

    open fun onDeleteClicked(position: Int) {}
}