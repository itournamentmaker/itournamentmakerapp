package com.littlebigpeople.itournamentmaker.util

import android.view.View
import android.view.ViewGroup
import java.util.*

/**
 * Created by daniel on 12/19/17.
 */

// Using BFS (Graph theory) to get all children of a view
// https://stackoverflow.com/questions/32652274/get-activitys-layout-from-a-fragment-and-block-all-its-childs
fun getAllChildrenBFS(v: View): List<View> {
    val visited = ArrayList<View>()
    val unvisited = ArrayList<View>()
    unvisited.add(v)

    while (!unvisited.isEmpty()) {
        val child = unvisited.removeAt(0)
        visited.add(child)
        if (child !is ViewGroup) continue
        val group = child
        val childCount = group.childCount
        for (i in 0 until childCount) unvisited.add(group.getChildAt(i))
    }
    return visited
}