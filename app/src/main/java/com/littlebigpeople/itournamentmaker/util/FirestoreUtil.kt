package com.littlebigpeople.itournamentmaker.util

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.littlebigpeople.itournamentmaker.data.Discipline
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.data.Tournament
import com.littlebigpeople.itournamentmaker.state.*
import com.littlebigpeople.itournamentmaker.tournament.AllVsAll
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit


/**
 * Created by danie on 12/14/2017.
 */

class FirestoreUtil {
    val fb: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val EXECUTOR = ThreadPoolExecutor(2, 4,
            60, TimeUnit.SECONDS, LinkedBlockingQueue<Runnable>())


    fun submitTeams(tournament: Tournament, teams: List<Team>) {

        val auth = FirebaseAuth.getInstance()
        // for each team ->
        // Create team in FS
        // Add player  in fs tour->team->player collection

        for (team in teams) {
            fb.collection("Tournaments").document(tournament.tournamentId).collection(TEAMS).document(team.uuid).set(team)

            for (player in team.players) {
                Log.d("id", player.id)
                val teamname = team.name
                val teamUid = team.uuid
                if (player.id != "") {
                    fb.collection(TOURNAMENTS).document(tournament.tournamentId).collection(TEAMS).document(team.uuid).collection("Players").document(player.id).set(player)
                    fb.collection(TOURNAMENTS).document(tournament.tournamentId).collection(PLAYERS).document(player.id).update(
                            "teamName", team.name, "teamUuid", team.uuid

                    )
                }
            }
        }
    }

    fun createSchedule(tournamentId: String?) {

        if (tournamentId != null) {
            fb.collection("Tournaments").document(tournamentId).collection("Teams").get()
                    .addOnSuccessListener { p0 ->
                        if (p0 != null) {
                            val teamDTOs: MutableList<Team> = p0.toObjects(Team::class.java)
                            val teams = ArrayList<Team>()
                            for (team in teamDTOs) {
                                teams.add(team)
                            }
                            getDisciplines(tournamentId, teams)
                        }
                    }.addOnFailureListener {

                    }
        }
    }

    fun getDisciplines(tournamentId: String?, teams: MutableList<Team>) {
        if (tournamentId != null) {
            fb.collection(TOURNAMENTS).document(tournamentId).collection(DISCIPLINES).get()
                    .addOnSuccessListener { p0 ->
                        if (p0 != null) {
                            val disciplines: MutableList<Discipline> = p0.toObjects(Discipline::class.java)
                            val schedule = AllVsAll().generateSchedule(teams, disciplines)

                            for (match in schedule) {
                                fb.collection(TOURNAMENTS).document(tournamentId).collection(MATCHES).document(match.matchId).set(match)
                            }
                        }
                    }.addOnFailureListener {

                    }
        }
    }


}