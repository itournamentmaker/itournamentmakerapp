package com.littlebigpeople.itournamentmaker.util


import com.littlebigpeople.itournamentmaker.data.Player
import com.littlebigpeople.itournamentmaker.data.Team
import java.util.*

interface TeamAssigner{

    fun generateTeams(players: MutableList<Player>, teamCount: Int, tournamentId: String): List<Team> {
        //Default implementation: random teams
        val teams = ArrayList<Team>(teamCount)

        for (i in 1..teamCount){
            teams.add(Team(UUID.randomUUID().toString(),ArrayList<Player>(), "Team "+i,0,tournamentId))
        }

        // This suffers from Java-ism
        var i : Int = 0
        for (player in players){
            teams[i].players.add(player)
            if (i < teamCount -1) i++
            else i = 0
        }

        return teams
    }

}