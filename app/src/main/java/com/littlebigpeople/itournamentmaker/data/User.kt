package com.littlebigpeople.itournamentmaker.data

/**
 * Created by dennisschmock on 01/12/2017.
 */
data class User(val name: String, val mail: String, val uuid: String, var tournaments: MutableList<Tournament> ) {
    constructor(): this("","","", mutableListOf())
}