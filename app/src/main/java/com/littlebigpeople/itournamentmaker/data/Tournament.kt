package com.littlebigpeople.itournamentmaker.data

import java.util.*

/**
 * Created by daniel on 12/12/17.
 */


class Tournament(var name:String,
                 var tournamentId:String,
                 val userId: String,
                 val username:String,
                 var playerCount:Int,
                 val teams: MutableList<Team>,
                 val tournamentState: TournamentState,
                 var tournamentType: TournamentType,
                 var openForPlayers: Boolean,
                 var teamsAssigned: Boolean,
                 var bracketsGenerated: Boolean,
                 var playersPrTeam: Int,
                 var teamNames:TeamNameOptions){

    constructor() : this("","","","", 0, ArrayList<Team>(),TournamentState.PREPARE,TournamentType.AllVsAll,true,false,false,0,TeamNameOptions.SEQUENTIAL)
    constructor(name:String,tournamentId:String,userId:String,username: String,playerCount:Int,tournamentState: TournamentState):this(name,tournamentId,userId,username,playerCount, mutableListOf<Team>(),tournamentState,TournamentType.AllVsAll,true,false,false,0,TeamNameOptions.SEQUENTIAL)
    constructor(name:String, tournamentId:String, userId:String,username: String, playerCount:Int, teams: MutableList<Team>):this(name,tournamentId,userId,username,playerCount, teams,TournamentState.PREPARE,TournamentType.AllVsAll,true,false,false,0,TeamNameOptions.SEQUENTIAL)

}