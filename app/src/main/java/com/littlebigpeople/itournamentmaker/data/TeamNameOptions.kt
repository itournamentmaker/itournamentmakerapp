package com.littlebigpeople.itournamentmaker.data

enum class TeamNameOptions {
    SEQUENTIAL,RANDOM
}