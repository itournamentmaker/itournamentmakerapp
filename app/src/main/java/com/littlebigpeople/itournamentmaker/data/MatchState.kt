package com.littlebigpeople.itournamentmaker.data

/**
 * Created by Dennis on 17-12-2017.
 */
enum class MatchState {
        READY,PLAYED
}