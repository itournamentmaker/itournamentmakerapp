package com.littlebigpeople.itournamentmaker.data

enum class TournamentType(val value: Int) {
    AllVsAll(1), Swiss(2);

    companion object {
        fun valueOf(value: Int): TournamentType? = TournamentType.values().find { it.value == value }
    }
}

