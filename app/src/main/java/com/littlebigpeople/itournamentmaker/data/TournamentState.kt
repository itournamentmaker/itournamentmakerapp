package com.littlebigpeople.itournamentmaker.data

/**
 * Created by Dennis on 29-12-2017.
 */
enum class TournamentState {
    PREPARE, PROGRESS, FINISHED
}