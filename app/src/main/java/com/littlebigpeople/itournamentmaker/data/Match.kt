package com.littlebigpeople.itournamentmaker.data

/**
 * Created by daniel on 12/12/17.
 */

data class Match(var state: MatchState, val matchId: String, val teamA: Team, val teamB: Team, var scoreA: Int, var scoreB: Int, val discipline: Discipline) {

    constructor(): this(MatchState.READY,"",Team(),Team(),0,0,Discipline())


}