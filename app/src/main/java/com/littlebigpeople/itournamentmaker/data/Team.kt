package com.littlebigpeople.itournamentmaker.data

import java.util.*

/**
 * Created by danie on 12/11/2017.
 */
class Team(val uuid: String, val players: MutableList<Player>, val name: String, var points: Int = 0, var tournamentId: String) {

    constructor() : this("",ArrayList<Player>(),"", 0, "")

}