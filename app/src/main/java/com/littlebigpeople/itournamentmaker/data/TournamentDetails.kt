package com.littlebigpeople.itournamentmaker.data

/**
 * Created by daniel on 12/12/17.
 */
data class TournamentDetails(val name: String, val user: String) {
    constructor() : this("","")
}