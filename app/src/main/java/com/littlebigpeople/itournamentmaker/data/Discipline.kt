package com.littlebigpeople.itournamentmaker.data

import com.google.firebase.firestore.GeoPoint

/**
 * Created by dennisschmock on 01/12/2017.
 */
data class Discipline(var name:String, var description: String,var uuid: String,val locationName:String,val coordinates:GeoPoint) {

    constructor() : this ("","","","",GeoPoint(0.0,0.0))
    constructor(name:String,description: String,uuid: String):this(name,description,uuid,"",GeoPoint(0.0,0.0))
}