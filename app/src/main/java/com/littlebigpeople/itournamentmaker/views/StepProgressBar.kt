package com.littlebigpeople.itournamentmaker.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.widget.ProgressBar
import org.jetbrains.anko.toast
import android.util.TypedValue
import android.util.DisplayMetrics
import android.util.SparseArray
import android.view.MotionEvent
import com.littlebigpeople.itournamentmaker.R
import android.R.attr.action


class StepProgressBar @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attrs, defStyleAttr) {

    private var mSize: Int = 0
    private var mPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var radius: Float = measuredHeight.toFloat()

    //Which step is being performed?
    private var myCurrentStep: Int = 0

    //The list of steps
    var steps: ArrayList<String> = arrayListOf()

    //Animation preparation
    private var mY = 0f
    private val mSteps = HashSet<CircleArea>(3)
    private val stepPointer = SparseArray<CircleArea>(3)

    private val INVALID_POINTER_ID = -1
    private var mActivePointerId = INVALID_POINTER_ID

    //touch part
    private var mPosX: Float = 0f
    private var mPosY: Float = 0f

    private var mLastTouchX = 0f
    private var mLastTouchY = 0f



    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        radius = measuredHeight / 2.7f
        mY = radius + paddingTop

        // drawLine(canvas)
        drawCircles(canvas, 3, radius)


    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val xPad = paddingLeft + paddingRight
        val yPad = paddingBottom + paddingTop
        val width = measuredWidth - xPad
        val height = dipToPixels(context,60f)

        setMeasuredDimension(measuredWidth, height.toInt()+20)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

    }

    init {
        mPaint.color = Color.RED
        mPaint.style = Paint.Style.STROKE
        mPaint.strokeWidth = 8f
        mPaint.strokeCap = Paint.Cap.BUTT
        radius = measuredHeight.toFloat()
        setPadding(paddingLeft + 50, paddingTop, paddingRight + 50, paddingBottom + 20)
    }

    private fun drawLine(canvas: Canvas, startX: Float, endX: Float, completed: Boolean) {
        var paint = Paint(Paint.ANTI_ALIAS_FLAG)
        if (completed) {
            paint.color = ContextCompat.getColor(context, R.color.colorPrimaryDark)
        } else {
            paint.color = Color.LTGRAY
        }
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 8f
        //val startX = paddingLeft.toFloat()
        //val endX = measuredWidth - paddingRight.toFloat()
        canvas.drawLine(startX, mY, endX, mY, paint)
    }

    private fun populateCircleMap(numberOfCircles: Int) {

    }

    private fun drawCircles(canvas: Canvas, numberOfCircles: Int, radius: Float) {


        val spaceBetweenCenterOfCircles = (measuredWidth - paddingLeft - paddingRight - radius * 2) / (numberOfCircles - 1)
        var circleX = paddingLeft + radius
        for ((index, step) in steps.withIndex()) {

            var current: Boolean = false
            var completed: Boolean = false
            if (index == myCurrentStep - 1) {
                current = true
            } else if (index < myCurrentStep) {
                completed = true
            }
            if (index < steps.size - 1) {


                var startX = circleX + radius + 6f
                var endX = startX + spaceBetweenCenterOfCircles - 2 * radius - 12f
                drawLine(canvas, startX, endX, completed)
            }
            drawCircle(canvas, circleX, mY, radius, (index + 1).toString(), current, step, completed)

            circleX = circleX + spaceBetweenCenterOfCircles
        }

    }

    private fun drawCircle(canvas: Canvas, x: Float, y: Float, radius: Float, stepNumber: String, current: Boolean, label: String, completed: Boolean) {
        var _stepNumber = stepNumber
        var paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = ContextCompat.getColor(context, R.color.colorPrimary)
        paint.style = Paint.Style.FILL

        when {
            completed -> {
                paint.color = ContextCompat.getColor(context, R.color.colorPrimaryDark)
                _stepNumber = "\u2713"
            }
        }
        canvas.drawCircle(x, y, radius, paint)


        paint.strokeWidth = 8f

        paint.style = Paint.Style.STROKE
        when {
            current -> paint.color = ContextCompat.getColor(context, R.color.colorPrimaryDark)
            completed -> {
                paint.color = ContextCompat.getColor(context, R.color.colorPrimaryDark)
            }
            else -> paint.color = Color.LTGRAY
        }
        canvas.drawCircle(x, y, radius - 4f, paint)


        var textPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        textPaint.color = Color.WHITE
        textPaint.strokeWidth = 4f
        textPaint.textSize = dipToPixels(context, 16F)
        textPaint.textAlign = Paint.Align.CENTER
        canvas.drawText(_stepNumber, x, y + 16f, textPaint)
        textPaint.color = Color.BLACK
        textPaint.textSize = dipToPixels(context, 14F)
        textPaint.textAlign = Paint.Align.CENTER
        canvas.drawText(label, x, y + radius * 1.6f, textPaint)


    }

    fun dipToPixels(context: Context, dipValue: Float): Float {
        val metrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics)
    }

    private class CircleArea(radius: Float, centerX: Int, centerY: Int) {

    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val action = event.action
        when (action) {
            MotionEvent.ACTION_DOWN -> {
                val x = event.x
                val y = event.y

                mLastTouchX = x
                mLastTouchY = y

                mActivePointerId = event.getPointerId(0)
            }
            MotionEvent.ACTION_MOVE -> {
                val x = event.x
                val y = event.y

                val dx = x - mLastTouchX
                val dy = y - mLastTouchY

                mPosX += dx
                mPosY += dy

                mLastTouchX = x
                mLastTouchY = y
                invalidate()

            }
            MotionEvent.ACTION_UP -> {
                mActivePointerId = INVALID_POINTER_ID
            }
            MotionEvent.ACTION_CANCEL -> {
                mActivePointerId = INVALID_POINTER_ID
            }

            MotionEvent.ACTION_POINTER_UP -> {
                val pointerIndex = action and MotionEvent.ACTION_POINTER_INDEX_MASK shr MotionEvent.ACTION_POINTER_INDEX_SHIFT
                val pointerId = event.getPointerId(pointerIndex)
                if (pointerId == mActivePointerId) {
                    val newPointerIndex = if (pointerIndex == 0) 1 else 0
                    mLastTouchX = event.getX(newPointerIndex)
                    mLastTouchY = event.getY(newPointerIndex)
                    mActivePointerId = event.getPointerId(newPointerIndex)
                }

            }

        }
        return true
    }

    public fun setCurrentStep(step: Int) {
        if (step > steps.size) {
            myCurrentStep = steps.size
        } else myCurrentStep = step

    }
}