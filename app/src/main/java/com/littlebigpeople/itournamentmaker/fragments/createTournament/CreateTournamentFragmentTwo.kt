package com.littlebigpeople.itournamentmaker.fragments.createTournament

import android.app.AlertDialog
import android.app.Fragment
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.activities.CreateTournament
import com.littlebigpeople.itournamentmaker.adapters.MyDisciplineAdapter
import com.littlebigpeople.itournamentmaker.adapters.controllers.SwipeHelps
import com.littlebigpeople.itournamentmaker.adapters.controllers.SwipeHelps.UnderlayButtonClickListener
import com.littlebigpeople.itournamentmaker.data.Discipline
import com.littlebigpeople.itournamentmaker.ui.DividerItemDecoration
import com.littlebigpeople.itournamentmaker.viewmodels.TournamentVM
import kotlinx.android.synthetic.main.fragment_create_tournament_two.*
import java.util.*


/**
 * Created by Dennis on 27-11-2017.
 */
class CreateTournamentFragmentTwo : Fragment() {

    private lateinit var viewAdapter: MyDisciplineAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var swipeHelps: SwipeHelps


    private val tournamentViewModel by lazy {
        ViewModelProviders.of(activity as CreateTournament).get(TournamentVM::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        Log.d("crate", "no")

        return inflater.inflate(R.layout.fragment_create_tournament_two, container, false)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_next.setOnClickListener {
            val fragmentThree = CreateTournamentFragmentThree()
            (activity as CreateTournament).navigateToFragment(fragmentThree)
            recoverSwiped()

        }

        fab_add_discipline.setOnClickListener {
            if (swipeHelps.swipedPos < 0) {
                createDisciplineDialog(-1)

            } else recoverSwiped()
        }
        btn_back.setOnClickListener {
            fragmentManager.popBackStack()
        }

        stepProgressBar.steps.add("Name/Type")
        stepProgressBar.steps.add("Disciplines")
        stepProgressBar.steps.add("Settings")
        stepProgressBar.setCurrentStep(2)
        setupRecyclerView()
    }


    private fun setupRecyclerView() {

        swipeHelps = object : SwipeHelps(activity, recyclerView) {
            override fun instantiateUnderlayButton(viewHolder: RecyclerView.ViewHolder, underlayButtons: List<UnderlayButton>) {
                (underlayButtons as ArrayList).add(UnderlayButton("DELETE",
                        0,
                        Color.parseColor("#FF3C30"), UnderlayButtonClickListener {
                    val alertDialog = AlertDialog.Builder(activity).setTitle("Delete Discipline?")
                    alertDialog.setPositiveButton("Delete") { _, _ ->
                        tournamentViewModel.disciplines.removeAt(it)
                        recyclerView.adapter.notifyItemRemoved(it)
                    }
                    alertDialog.setNegativeButton("Cancel") { _, _ -> }
                    alertDialog.create().show()

                }))
                (underlayButtons as ArrayList).add(UnderlayButton("EDIT",
                        0,
                        Color.BLUE, UnderlayButtonClickListener {
                    createDisciplineDialog(it)

                    recoverQueue.add(swipedPos)
                    swipedPos = -1
                    recoverSwipedItem()
                }))

            }
        }

        val itemDecorator = DividerItemDecoration(this.activity, DividerItemDecoration.VERTICAL_LIST)

        viewManager = LinearLayoutManager(activity)
        viewAdapter = MyDisciplineAdapter(tournamentViewModel.disciplines)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = viewManager
        recyclerView.adapter = viewAdapter


        recyclerView.addItemDecoration(itemDecorator)

    }


    private fun createDisciplineDialog(pos: Int) {

        var discipline: Discipline = Discipline()
        if (pos >= 0) {
            discipline = tournamentViewModel.disciplines[pos]
        }
        val inflater = activity.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.dialog_create_discipline, null)
        val name = dialogView.findViewById(R.id.input_discipline_name) as EditText
        val description = dialogView.findViewById(R.id.input_discipline_description) as EditText
        if (discipline.name != "") name.setText(discipline.name)
        if (discipline.description != "") description.setText(discipline.description)

        val alertDialog = AlertDialog.Builder(activity)
                .setCancelable(true)

                .setView(dialogView)
                .setPositiveButton(R.string.save) { _, _ -> }
                .setNegativeButton(R.string.cancel) { p0, _ ->
                    p0.cancel()
                }.create()

        alertDialog.show()


        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            val disciplineName = name.text.trim()
            if (disciplineName.length < 3) {
                name.setText(disciplineName)
                name.error = getString(R.string.error_three_characters)
            } else {

                if (pos >= 0) {
                    discipline.name = name.text.toString().trim()
                    discipline.description = description.text.toString().trim()
                    tournamentViewModel.disciplines[pos] = discipline
                    recyclerView.adapter.notifyItemChanged(pos)

                } else {
                    val uuid = UUID.randomUUID().toString()
                    discipline = Discipline(name.text.toString(),description.text.toString().trim(),uuid)
                    tournamentViewModel.disciplines.add(discipline)
                    recyclerView.adapter.notifyItemInserted(tournamentViewModel.disciplines.size)

                }

                alertDialog.dismiss()
            }
        }


    }

    private fun recoverSwiped() {
        swipeHelps.recoverQueue.add(swipeHelps.swipedPos)
        swipeHelps.swipedPos = -1
        swipeHelps.recoverSwipedItem()
    }


}