package com.littlebigpeople.itournamentmaker.fragments

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.adapters.JoinedTeamAdapter
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.state.Ids
import com.littlebigpeople.itournamentmaker.state.TEAMS
import com.littlebigpeople.itournamentmaker.state.TOURNAMENTS
import com.littlebigpeople.itournamentmaker.ui.DividerItemDecoration
import com.littlebigpeople.itournamentmaker.viewholders.TeamViewHolder
import kotlinx.android.synthetic.main.fragment_list_joined_teams.*

/**
 * Created by dennisschmock on 11/12/2017.
 */
class ListJoinedTeamsFragment : Fragment() {

    var mAdapter: FirestoreRecyclerAdapter<Team, TeamViewHolder>? = null
    var tournamentId = ""
    val fb = FirebaseFirestore.getInstance()
   // var tournament: Tournament? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        //TODO Use arguments
        tournamentId = Ids.currentJoinedTournament!!
        return inflater.inflate(R.layout.fragment_list_joined_teams, container, false)
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).enableViews(true)

        val query: Query = fb.collection(TOURNAMENTS).document(tournamentId).collection(TEAMS)
        val options = FirestoreRecyclerOptions.Builder<Team>().setQuery(query, Team::class.java).build()
        setupRecyclerViewOptions(options)
    }

    fun setupRecyclerViewOptions(options: FirestoreRecyclerOptions<Team>) {
        mAdapter = JoinedTeamAdapter(options, this)
        val itemDecorator = DividerItemDecoration(this.activity, DividerItemDecoration.VERTICAL_LIST)
        rv_teams_joined.layoutManager = LinearLayoutManager(this.activity)
        rv_teams_joined.setHasFixedSize(true)
        rv_teams_joined.adapter = mAdapter
        rv_teams_joined.addItemDecoration(itemDecorator)

        (mAdapter as JoinedTeamAdapter).startListening()
    }

    override fun onStart() {
        super.onStart()

        if (mAdapter != null) {
            (mAdapter as JoinedTeamAdapter).startListening()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mAdapter != null) {
            (mAdapter as JoinedTeamAdapter).stopListening()
        }
    }



}