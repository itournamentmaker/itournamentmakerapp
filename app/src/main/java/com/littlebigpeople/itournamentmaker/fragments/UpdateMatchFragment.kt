package com.littlebigpeople.itournamentmaker.fragments

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.*
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.data.Match
import com.littlebigpeople.itournamentmaker.data.MatchState
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.state.*
import kotlinx.android.synthetic.main.fragment_update_match.*
import org.jetbrains.anko.toast


/**
 * Created by Dennis on 20-12-2017.
 */
class UpdateMatchFragment : Fragment(), EventListener<DocumentSnapshot> {
    private var mMatchListener: ListenerRegistration? = null
    private var mMatchRef: DocumentReference? = null
    private val db = FirebaseFirestore.getInstance()
    lateinit var tournamentId: String
    lateinit var matchId: String
    lateinit var match: Match

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        tournamentId = this.arguments.getString(TOURNAMENT_ID)
        matchId = this.arguments.getString(MATCH_ID)
        return inflater.inflate(R.layout.fragment_update_match, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mMatchRef = db.collection(TOURNAMENTS).document(tournamentId).collection(MATCHES).document(matchId)

        setupButtons()

    }

    override fun onEvent(documentSnapshot: DocumentSnapshot?, e: FirebaseFirestoreException?) {
        toast("event")
        if (e != null) {
            //todo handle excemption
            toast("error")
            return
        } else {
            toast("event")

            if (documentSnapshot != null && documentSnapshot.exists()) onMatchLoaded(documentSnapshot.toObject(Match::class.java))
        }

    }

    private fun setupButtons() {
        btn_save_match.setOnClickListener {
            updatePoints()
        }
    }

    private fun updatePoints() {
        (activity as MainActivity).showSpinner()
        var teamAPoints = 0
        var teamBPoints = 0

        if (score_team_a.text.toString() == "") {
            match.scoreA = 0
        } else {
            match.scoreA = Integer.parseInt(score_team_a.text.toString())
        }

        if (score_team_a.text.toString() == "") {
            match.scoreB = 0
        } else {
            match.scoreB = Integer.parseInt(score_team_b.text.toString())
        }


        match.state = MatchState.PLAYED
        if (match.scoreA > match.scoreB) {
            teamAPoints = 3
            teamBPoints = 0
        } else if (match.scoreA < match.scoreB) {
            teamAPoints = 0
            teamBPoints = 3
        } else {
            teamAPoints = 1
            teamBPoints = 1
        }

        //todo convert to transactions
        db.collection(TOURNAMENTS).document(tournamentId).collection(MATCHES).document(matchId).set(match).addOnSuccessListener {

            db.collection(TOURNAMENTS).document(tournamentId).collection(TEAMS).document(match.teamA.uuid).get()
                    .addOnSuccessListener(object : OnSuccessListener<DocumentSnapshot> {
                        override fun onSuccess(snap: DocumentSnapshot?) {
                            if (snap != null) {
                                val teamA = snap.toObject(Team::class.java)
                                teamA.points = teamA.points + teamAPoints
                                db.collection(TOURNAMENTS).document(tournamentId).collection(TEAMS).document(teamA.uuid).set(teamA)


                                db.collection(TOURNAMENTS).document(tournamentId).collection(TEAMS).document(match.teamB.uuid).get()
                                        .addOnSuccessListener(object : OnSuccessListener<DocumentSnapshot> {
                                            override fun onSuccess(snap: DocumentSnapshot?) {
                                                if (snap != null) {
                                                    val teamB = snap.toObject(Team::class.java)
                                                    teamB.points = teamB.points + teamBPoints
                                                    db.collection(TOURNAMENTS).document(tournamentId).collection(TEAMS).document(teamB.uuid).set(teamB)

                                                }
                                                (activity as MainActivity).hideSpinner()

                                                (activity as MainActivity).onBackPressed()
                                            }

                                        }).addOnFailureListener {
                                    (activity as MainActivity).hideSpinner()

                                    (activity as MainActivity).onBackPressed()
                                }
                            }
                        }

                    }).addOnFailureListener {
                (activity as MainActivity).hideSpinner()

                (activity as MainActivity).onBackPressed()
            }

        }.addOnFailureListener {
            (activity as MainActivity).hideSpinner()

            (activity as MainActivity).onBackPressed()
        }
    }

    private fun onMatchLoaded(match: Match) {

        text_match_discipline_name.text = match.discipline.name
        text_team_a.text = match.teamA.name
        text_team_b.text = match.teamB.name
    }

    override fun onStart() {
        super.onStart()
        mMatchListener = mMatchRef?.addSnapshotListener(this)
    }

    override fun onStop() {
        super.onStop()
        if (mMatchListener != null) {
            (mMatchListener as ListenerRegistration).remove()
            mMatchListener = null
        }

    }

}