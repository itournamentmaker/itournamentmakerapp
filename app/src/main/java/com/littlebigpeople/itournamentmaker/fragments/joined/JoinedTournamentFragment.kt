package com.littlebigpeople.itournamentmaker.fragments.joined

import android.app.Fragment
import android.app.FragmentTransaction
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.components.PlayerComponent
import com.littlebigpeople.itournamentmaker.components.TournamentComponent
import com.littlebigpeople.itournamentmaker.data.Player
import com.littlebigpeople.itournamentmaker.data.Tournament
import com.littlebigpeople.itournamentmaker.fragments.ListJoinedTeamsFragment
import com.littlebigpeople.itournamentmaker.fragments.ListScoresFragment
import com.littlebigpeople.itournamentmaker.state.Ids
import com.littlebigpeople.itournamentmaker.state.PLAYED_MATCHES
import com.littlebigpeople.itournamentmaker.state.PLAYER_ID
import com.littlebigpeople.itournamentmaker.state.TOURNAMENT_ID
import com.littlebigpeople.itournamentmaker.util.getAllChildrenBFS
import kotlinx.android.synthetic.main.fragment_joined_tournament.*
import org.jetbrains.anko.toast
import java.util.*


/**
 * Created by daniel on 12/8/17.
 */
class JoinedTournamentFragment : Fragment(), Observer {


    val fb: FirebaseFirestore = FirebaseFirestore.getInstance()
    val auth = FirebaseAuth.getInstance()
    val tc = TournamentComponent()
    val pc = PlayerComponent()
    lateinit var views: List<View>
    lateinit var mainView: View
    lateinit var tournament: Tournament
    lateinit var playerId: String
    lateinit var tournamentId: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_joined_tournament, container, false)
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //tournamentId = this.arguments.getString("id")

        playerId = Ids.currentJoinedPlayer!!
        tournamentId = Ids.currentJoinedTournament!!
        mainView = view!!
        views = view?.let { getAllChildrenBFS(it) }!!
        Ids.currentJoinedTournament?.let { tc.getTournament(it) }

        if (!tournamentId.isNullOrBlank() && !playerId.isNullOrBlank()) {

            pc.getPlayer(tournamentId, playerId)
            (activity as MainActivity).showSpinner()
            //showView(mainLayout)
        } else {
            showView(addPlayerLayout)
        }

        activity.title = getString(R.string.dashboard)
        (activity as MainActivity).enableViews(false)


        setOnClickListeners()
    }

    override fun onResume() {
        super.onResume()
        tc.addObserver(this)
        pc.addObserver(this)
    }

    fun setOnClickListeners() {
        btn_list_teams.setOnClickListener {
            val bundle = Bundle()
            val listJoinedTeamsFragment = ListJoinedTeamsFragment()
            listJoinedTeamsFragment.arguments = bundle

            val ft: FragmentTransaction = fragmentManager.beginTransaction()
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)

            ft.replace(R.id.flContent, listJoinedTeamsFragment).addToBackStack(null).commit()

        }

        btn_list_players.setOnClickListener {

            val listAllJoinedPlayersFragment = ListJoinedAllPlayersFragment()
            val ft: FragmentTransaction = fragmentManager.beginTransaction()
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)

            ft.replace(R.id.flContent, listAllJoinedPlayersFragment).addToBackStack(null).commit()

        }


        btn_joined_main.setOnClickListener {
            showView(mainLayout)
        }

        btn_submit_name.setOnClickListener {
            val player: Player = Player(UUID.randomUUID().toString(), (edittext_enter_name as EditText).text.toString(), "", "")
            Ids.setCurentJoinedPlayer(activity, player.id)
            pc.addPlayerToTournament(tournamentId, player)
            (activity as MainActivity).showSpinner()

        }

        btn_list_results.setOnClickListener {
            val bundle = Bundle()

            bundle.putBoolean(PLAYED_MATCHES, true)
            bundle.putString(PLAYER_ID, playerId)
            val listJoinedMatchesFragment = ListJoinedMatchesFragment()
            listJoinedMatchesFragment.arguments = bundle

            val ft: FragmentTransaction = fragmentManager.beginTransaction()
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)

            ft.replace(R.id.flContent, listJoinedMatchesFragment).addToBackStack(null).commit()

        }

        btn_list_unplayed.setOnClickListener {
            val listJoinedMatchesFragment = ListJoinedMatchesFragment()
            val bundle = Bundle()

            bundle.putBoolean(PLAYED_MATCHES, false)
            listJoinedMatchesFragment.arguments = bundle
            val ft: FragmentTransaction = fragmentManager.beginTransaction()
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)

            ft.replace(R.id.flContent, listJoinedMatchesFragment).addToBackStack(null).commit()

        }

        btn_joined_scoreboard.setOnClickListener {
            val fragment = ListScoresFragment()
            val bundle = Bundle()
            bundle.putString(TOURNAMENT_ID, tournamentId)

            fragment.arguments = bundle

            val ft: FragmentTransaction = fragmentManager.beginTransaction()
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)

            ft.replace(R.id.flContent, fragment).addToBackStack(null).commit()
        }
    }

    fun setAllToGone() {
        for (view in views) {
            view.visibility = View.GONE
        }
        outerLayout.visibility = View.VISIBLE
    }


    fun showView(v: View) {
        setAllToGone()

        val visited = ArrayList<View>()
        val unvisited = ArrayList<View>()
        unvisited.add(v)

        while (!unvisited.isEmpty()) {
            val child = unvisited.removeAt(0)
            visited.add(child)
            if (child !is ViewGroup) continue
            val group = child as ViewGroup
            val childCount = group.childCount
            for (i in 0 until childCount) unvisited.add(group.getChildAt(i))
        }

        for (v in visited) {
            v.visibility = View.VISIBLE
        }
    }


    override fun update(obs: Observable?, arg: Any?) {

        if (arg is Tournament) {
            tournament =arg
            textview_addPlayer_tournamentname.text = tournament.name
            textview_joined_name.text = tournament.name
            setOnClickListeners()
        }

        if (arg is Player) {

            val player = arg
            val_player_name.text = player.name
            if (player.teamName != null) {
                val_my_team_name.text = "Team: " + player.teamName
            }
            Ids.setCurentJoinedPlayer(activity, player.id)
            showView(mainLayout)
            (activity as MainActivity).hideSpinner()

        } else if (arg is Exception) {
            toast("Error")
        }
    }

    override fun onStop() {
        super.onStop()
        tc.deleteObserver(this)
        pc.deleteObserver(this)
    }

}