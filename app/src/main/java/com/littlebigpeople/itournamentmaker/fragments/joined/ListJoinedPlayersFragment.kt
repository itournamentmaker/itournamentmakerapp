package com.littlebigpeople.itournamentmaker.fragments

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.adapters.PlayersAdapter
import com.littlebigpeople.itournamentmaker.data.Player
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.state.*
import com.littlebigpeople.itournamentmaker.ui.DividerItemDecoration
import com.littlebigpeople.itournamentmaker.viewholders.PlayerViewHolder
import kotlinx.android.synthetic.main.fragment_list_joined_players.*

/**
 * Created by dennisschmock on 08/12/2017.
 */
class ListJoinedPlayersFragment : Fragment() {
    var mAdapter: FirestoreRecyclerAdapter<Player, PlayerViewHolder>? = null
    lateinit var teamUuid: String
    var team: Team? = null
    val fb = FirebaseFirestore.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        teamUuid = arguments.getString(TEAM_ID)
        return inflater.inflate(R.layout.fragment_list_joined_players, container, false)

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).enableViews(true)

        //todo simplify this
        if (Ids.currentJoinedTournament != null && teamUuid.isEmpty()) {
            val query: Query = FirebaseFirestore.getInstance().collection(TOURNAMENTS).document(Ids.currentJoinedTournament as String).collection(PLAYERS)
            val options = FirestoreRecyclerOptions.Builder<Player>().setQuery(query, Player::class.java).build()
            setupRecyclerView(options)
        } else if (Ids.currentJoinedTournament != null && !teamUuid.isEmpty()) {
            val query: Query = FirebaseFirestore.getInstance().collection(TOURNAMENTS).document(Ids.currentJoinedTournament as String).collection(TEAMS).document(teamUuid).collection("Players")
            val options = FirestoreRecyclerOptions.Builder<Player>().setQuery(query, Player::class.java).build()
            setupRecyclerView(options)
        }

    }

    fun setupRecyclerView(options: FirestoreRecyclerOptions<Player>) {
        mAdapter = PlayersAdapter(options, this)
        val itemDecorator = DividerItemDecoration(this.activity, DividerItemDecoration.VERTICAL_LIST)

        rv_joined_players.layoutManager = LinearLayoutManager(this.activity)
        rv_joined_players.setHasFixedSize(true)
        rv_joined_players.adapter = mAdapter
        rv_joined_players.addItemDecoration(itemDecorator)
        if (mAdapter != null) {
            mAdapter?.startListening()
        }
    }


}