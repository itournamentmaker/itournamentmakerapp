package com.littlebigpeople.itournamentmaker.fragments.admin

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.adapters.MatchesAdapter
import com.littlebigpeople.itournamentmaker.data.Match
import com.littlebigpeople.itournamentmaker.data.MatchState
import com.littlebigpeople.itournamentmaker.state.Ids
import com.littlebigpeople.itournamentmaker.state.MATCHES
import com.littlebigpeople.itournamentmaker.state.PLAYED_MATCHES
import com.littlebigpeople.itournamentmaker.state.TOURNAMENTS
import com.littlebigpeople.itournamentmaker.ui.DividerItemDecoration
import com.littlebigpeople.itournamentmaker.util.FirestoreUtil
import com.littlebigpeople.itournamentmaker.viewholders.MatchViewHolder
import kotlinx.android.synthetic.main.fragment_list_matches.*

/**
 * Created by Dennis on 17-12-2017.
 */
class ListMatchesFragment : Fragment() {

    var mAdapter: FirestoreRecyclerAdapter<Match, MatchViewHolder>? = null
    val fs = FirebaseFirestore.getInstance()
    var filterMatches = MatchState.READY

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        if (this.arguments != null) {
            if (this.arguments.getBoolean(PLAYED_MATCHES)) {
                filterMatches = MatchState.PLAYED
            }
        }

        return inflater.inflate(R.layout.fragment_list_matches, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).enableViews(true)
        (activity as MainActivity).showSpinner()


        val query: Query = FirebaseFirestore.getInstance().collection(TOURNAMENTS).document(Ids.currentAdminTournament!!).collection(MATCHES).whereEqualTo("state", filterMatches.toString())
        val options = FirestoreRecyclerOptions.Builder<Match>().setQuery(query, Match::class.java).build()

        setupRecyclerView(options)


        query.get().addOnSuccessListener { querySnapshot ->
            if (!querySnapshot.isEmpty) {
                btn_create_schedule.visibility = View.GONE

            }
            (activity as MainActivity).hideSpinner()
        }

        btn_create_schedule.setOnClickListener {



            FirestoreUtil().createSchedule(Ids.currentAdminTournament)
            btn_create_schedule.visibility = View.INVISIBLE

        }
    }



    fun setupRecyclerView(options: FirestoreRecyclerOptions<Match>) {
        mAdapter = MatchesAdapter(options, this, Ids.currentAdminTournament!!)
        val itemDecorator = DividerItemDecoration(this.activity, DividerItemDecoration.VERTICAL_LIST)

        rv_matches.layoutManager = LinearLayoutManager(this.activity)
        rv_matches.setHasFixedSize(true)
        rv_matches.adapter = mAdapter
        rv_matches.addItemDecoration(itemDecorator)
        if (mAdapter != null) {
            mAdapter?.startListening()
        }
    }


}