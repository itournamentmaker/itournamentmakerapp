package com.littlebigpeople.itournamentmaker.fragments

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.adapters.TournamentsAdapter
import com.littlebigpeople.itournamentmaker.data.Tournament
import com.littlebigpeople.itournamentmaker.ui.DividerItemDecoration
import com.littlebigpeople.itournamentmaker.viewholders.TournamentViewHolder
import kotlinx.android.synthetic.main.fragment_my_tournaments.*


/**
 * Created by Dennis on 27-11-2017.
 */
class ListTournamentsFragment : Fragment() {
    var mAdapter: FirestoreRecyclerAdapter<Tournament,TournamentViewHolder>? = null

    val fb = FirebaseFirestore.getInstance()
    val currentUserId = FirebaseAuth.getInstance().currentUser?.uid

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {


        return inflater.inflate(R.layout.fragment_my_tournaments, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(currentUserId!=null){
            val query: Query = FirebaseFirestore.getInstance().collection("Tournaments").whereEqualTo("userId",currentUserId)
            val options: FirestoreRecyclerOptions<Tournament> = FirestoreRecyclerOptions.Builder<Tournament>().setQuery(query, Tournament::class.java).build()

            setupRecyclerView(options)
        }

    }

    fun setupRecyclerView(options: FirestoreRecyclerOptions<Tournament>) {
        mAdapter = TournamentsAdapter(options,this)
        val itemDecorator = DividerItemDecoration(this.activity, DividerItemDecoration.VERTICAL_LIST )

        rv_tournaments.layoutManager = LinearLayoutManager(this.activity)
        rv_tournaments.setHasFixedSize(true)
        rv_tournaments.adapter = mAdapter
        rv_tournaments.addItemDecoration(itemDecorator)
        (activity as MainActivity).showSpinner()

        mAdapter?.startListening()

    }

    override fun onStart() {
        super.onStart()
        mAdapter?.startListening()


    }

    override fun onStop() {
        super.onStop()
        mAdapter?.stopListening()
    }

}


