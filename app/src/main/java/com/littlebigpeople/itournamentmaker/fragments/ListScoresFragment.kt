package com.littlebigpeople.itournamentmaker.fragments

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.adapters.ScoreAdapter
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.data.Tournament
import com.littlebigpeople.itournamentmaker.state.Ids
import com.littlebigpeople.itournamentmaker.ui.DividerItemDecoration
import com.littlebigpeople.itournamentmaker.viewholders.ScoreViewHolder
import kotlinx.android.synthetic.main.fragment_list_scores.*

/**
 * Created by dennisschmock on 11/12/2017.
 */
class ListScoresFragment : Fragment() {

    var mAdapter: FirestoreRecyclerAdapter<Team, ScoreViewHolder>? = null
    var admin = false
    val fb = FirebaseFirestore.getInstance()
    var tournament: Tournament? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        admin = this.arguments.getBoolean("isAdmin")

        return inflater.inflate(R.layout.fragment_list_scores, container, false)
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).enableViews(true)

        var tournamentId = ""

        if (Ids.currentJoinedTournament!=""){
            tournamentId = Ids.currentJoinedTournament as String
        } else if (Ids.currentAdminTournament!=""){
            tournamentId = Ids.currentAdminTournament as String
        } else return

        val query: Query = fb.collection("Tournaments").document(tournamentId).collection("Teams").orderBy("points",Query.Direction.DESCENDING)
        val options = FirestoreRecyclerOptions.Builder<Team>().setQuery(query, Team::class.java).build()
        setupRecyclerViewOptions(options)


    }

    fun setupRecyclerViewOptions(options: FirestoreRecyclerOptions<Team>) {
        mAdapter = ScoreAdapter(options, this)
        val itemDecorator = DividerItemDecoration(this.activity, DividerItemDecoration.VERTICAL_LIST)
        rv_scores.layoutManager = LinearLayoutManager(this.activity)
        rv_scores.setHasFixedSize(true)
        rv_scores.adapter = mAdapter
        rv_scores.addItemDecoration(itemDecorator)
        (mAdapter as ScoreAdapter).startListening()
    }

    override fun onStart() {
        super.onStart()

        if (mAdapter != null) {
            (mAdapter as ScoreAdapter).startListening()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mAdapter != null) {
            (mAdapter as ScoreAdapter).stopListening()
        }
    }




}


