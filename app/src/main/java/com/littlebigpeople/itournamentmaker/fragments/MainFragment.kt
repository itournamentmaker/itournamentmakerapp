package com.littlebigpeople.itournamentmaker.fragments

import android.app.Fragment
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseAuth
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.activities.CreateTournament
import com.littlebigpeople.itournamentmaker.fragments.joined.JoinedTournamentFragment
import com.littlebigpeople.itournamentmaker.state.Ids
import kotlinx.android.synthetic.main.fragment_main.*


/**
 * Created by Dennis on 27-11-2017.
 */
class MainFragment:Fragment() {

    val auth : FirebaseAuth? = FirebaseAuth.getInstance()
    lateinit var tournamentId: String
    lateinit var playerId: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_main,container,false)
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab_new_tournament.setOnClickListener{
            val intent = Intent(activity,CreateTournament::class.java)
            startActivity(intent)

        }

        if(/*arguments!=null&&arguments.getString(PLAYER_ID)!=null&&arguments.getString(TOURNAMENT_ID)!=null*/
        !Ids.currentJoinedPlayer.isNullOrEmpty() && !Ids.currentJoinedTournament.isNullOrEmpty()){
            //text_view_main_Screen.visibility = View.VISIBLE
            rejoinView.visibility = View.VISIBLE
            btn_goto_joined_tournament.setOnClickListener{
                val joinedTournamentFragment = JoinedTournamentFragment()
                fragmentManager.beginTransaction().replace(R.id.flContent, joinedTournamentFragment).addToBackStack("main").commit()
            }
        }
        else rejoinView.visibility = View.GONE


    }



}