package com.littlebigpeople.itournamentmaker.fragments.createTournament

import android.app.AlertDialog
import android.app.Fragment
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.activities.CreateTournament
import com.littlebigpeople.itournamentmaker.components.TournamentComponent
import com.littlebigpeople.itournamentmaker.data.TeamNameOptions
import com.littlebigpeople.itournamentmaker.data.Tournament
import com.littlebigpeople.itournamentmaker.viewmodels.TournamentVM
import kotlinx.android.synthetic.main.fragment_create_tournament_three.*
import org.jetbrains.anko.toast


/**
 * Created by Dennis on 27-11-2017.
 */
class CreateTournamentFragmentThree : Fragment(),TextWatcher {


    private val TC = TournamentComponent()


    private val tournamentViewModel by lazy {
        ViewModelProviders.of(activity as CreateTournament).get(TournamentVM::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {


        return inflater.inflate(R.layout.fragment_create_tournament_three, container, false)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setupButtons()

        setupState()


    }

    private fun setupState() {
        input_number_players_team.hint = tournamentViewModel.tournament.playersPrTeam.toString()

    }


    private fun setupButtons() {
        selector_team_name_left.setOnClickListener {
            selector_team_name_left.background = activity.getDrawable(R.drawable.button_selector_left_selected)
            selector_team_name_left.setTextColor(ContextCompat.getColor(activity, R.color.white))
            selector_team_name_right.background = activity.getDrawable(R.drawable.button_selector_right_deselected)
            selector_team_name_right.setTextColor(ContextCompat.getColor(activity, R.color.deselected_gray))
            tournamentViewModel.tournament.teamNames = TeamNameOptions.SEQUENTIAL
        }

        selector_team_name_right.setOnClickListener {
            selector_team_name_left.background = activity.getDrawable(R.drawable.button_selector_left_deselected)
            selector_team_name_left.setTextColor(ContextCompat.getColor(activity, R.color.deselected_gray))
            selector_team_name_right.background = activity.getDrawable(R.drawable.button_selector_right_selected)
            selector_team_name_right.setTextColor(ContextCompat.getColor(activity, R.color.white))
            tournamentViewModel.tournament.teamNames = TeamNameOptions.RANDOM
        }

        selector_players_join_left.setOnClickListener {
            selector_players_join_left.background = activity.getDrawable(R.drawable.button_selector_left_selected)
            selector_players_join_left.setTextColor(ContextCompat.getColor(activity, R.color.white))
            selector_players_join_right.background = activity.getDrawable(R.drawable.button_selector_right_deselected)
            selector_players_join_right.setTextColor(ContextCompat.getColor(activity, R.color.deselected_gray))
            tournamentViewModel.tournament.openForPlayers = true
        }

        selector_players_join_right.setOnClickListener {
            selector_players_join_left.background = activity.getDrawable(R.drawable.button_selector_left_deselected)
            selector_players_join_left.setTextColor(ContextCompat.getColor(activity, R.color.deselected_gray))
            selector_players_join_right.background = activity.getDrawable(R.drawable.button_selector_right_selected)
            selector_players_join_right.setTextColor(ContextCompat.getColor(activity, R.color.white))
            tournamentViewModel.tournament.openForPlayers = false

        }

        btn_next.setOnClickListener {
            tournamentViewModel.tournament.playersPrTeam = input_number_players_team.text.toString().toInt()
            TC.createTournament(tournamentViewModel.tournament,tournamentViewModel.disciplines) {
                toast("finished")
            }
        }

        btn_info_team_names.setOnClickListener {

            infoDialog("This is it")
        }

        btn_info_players_join.setOnClickListener {
            infoDialog("another one bites the dust")
        }

        btn_back.setOnClickListener {
            fragmentManager.popBackStack()
        }

        stepProgressBar.steps.add("Name/Type")
        stepProgressBar.steps.add("Disciplines")
        stepProgressBar.steps.add("Settings")
        stepProgressBar.setCurrentStep(3)

        //Make sure keyboards disappears on ENTER
        input_number_players_team.setOnKeyListener { view, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER) {

                (activity as CreateTournament).hideSoftKeyboard(activity)
                input_number_players_team.isFocusable = false
                input_number_players_team.isFocusableInTouchMode = true
                true
            } else {
                false
            }
        }
    }


    private fun infoDialog(message: String) {
        val alertDialogBuilder = AlertDialog.Builder(activity)
        alertDialogBuilder.setIcon(R.drawable.ic_info_black_24dp)
        alertDialogBuilder.setTitle("afd")
        alertDialogBuilder.setMessage(message)
        alertDialogBuilder.setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialogInterface, i -> })
        alertDialogBuilder.create().show()

    }


    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun afterTextChanged(p0: Editable?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}