package com.littlebigpeople.itournamentmaker.fragments.admin

import android.app.AlertDialog
import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.adapters.PlayersAdapter
import com.littlebigpeople.itournamentmaker.data.Player
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.state.Ids
import com.littlebigpeople.itournamentmaker.state.PLAYERS
import com.littlebigpeople.itournamentmaker.state.TOURNAMENTS
import com.littlebigpeople.itournamentmaker.state.TOURNAMENT_ID
import com.littlebigpeople.itournamentmaker.ui.DividerItemDecoration
import com.littlebigpeople.itournamentmaker.viewholders.PlayerViewHolder
import kotlinx.android.synthetic.main.fragment_list_players.*
import java.util.*

/**
 * Created by dennisschmock on 08/12/2017.
 */
class ListPlayersFragment : Fragment() {
    var mAdapter: FirestoreRecyclerAdapter<Player, PlayerViewHolder>? = null
    lateinit var tournamentId: String
    lateinit var teamUuid: String
    var team: Team? = null
    val fb = FirebaseFirestore.getInstance()



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        tournamentId = arguments.getString(TOURNAMENT_ID)

        return inflater.inflate(R.layout.fragment_list_players, container, false)

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).enableViews(true)


            val query: Query = FirebaseFirestore.getInstance().collection(TOURNAMENTS).document(tournamentId).collection(PLAYERS)
            val options = FirestoreRecyclerOptions.Builder<Player>().setQuery(query, Player::class.java).build()
            setupRecyclerView(options)


        btn_add_player.setOnClickListener {

            alertDialogBuilder()

        }
    }

    fun setupRecyclerView(options: FirestoreRecyclerOptions<Player>) {
        mAdapter = PlayersAdapter(options, this)
        val itemDecorator = DividerItemDecoration(this.activity, DividerItemDecoration.VERTICAL_LIST)

        rv_players.layoutManager = LinearLayoutManager(this.activity)
        rv_players.setHasFixedSize(true)
        rv_players.adapter = mAdapter
        rv_players.addItemDecoration(itemDecorator)
        if (mAdapter != null) {
            mAdapter?.startListening()
        }
    }

    fun alertDialogBuilder() {
        val input = EditText(activity)
        input.inputType = InputType.TYPE_CLASS_TEXT
        val addPlayerDialog =
                AlertDialog.Builder(activity)
                        .setCancelable(true)
                        .setTitle("Add Player")
                        .setView(input)
                        .setPositiveButton(getString(R.string.save)) { p0, p1 ->

                        }
                        .setNegativeButton(R.string.cancel) { p0, _ ->
                            p0.cancel()
                        }.create()


        addPlayerDialog.show()

        addPlayerDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            val playerName = input.text.toString().trim()
            if (playerName.length <= 3) {
                input.error = getString(R.string.input_player_name_error)
            } else {
                val pUuid = UUID.randomUUID()
                val player = Player(pUuid.toString(), playerName,"","")

                if (Ids.currentAdminTournament != null) {
                    fb.collection(TOURNAMENTS).document(Ids.currentAdminTournament!!).collection("Players").document(player.id).set(player)
                }


                addPlayerDialog.dismiss()

            }
        }
    }

}