package com.littlebigpeople.itournamentmaker.fragments.admin

import android.app.Fragment
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.components.TournamentComponent
import com.littlebigpeople.itournamentmaker.data.Tournament
import com.littlebigpeople.itournamentmaker.fragments.TournamentDashboardFragment
import com.littlebigpeople.itournamentmaker.state.Ids
import kotlinx.android.synthetic.main.fragment_create_tournament.*
import org.jetbrains.anko.toast
import java.lang.Exception
import java.util.*


/**
 * Created by Dennis on 27-11-2017.
 */
class CreateTournamentFragment : Fragment(), Observer {


    val fb: FirebaseFirestore = FirebaseFirestore.getInstance()
    val currentUser = FirebaseAuth.getInstance().currentUser
    val tc = TournamentComponent()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        tc.addObserver(this)
        return inflater.inflate(R.layout.fragment_create_tournament_one, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_next_step.setOnClickListener {
            var input = input_tournament_title.text.toString().trim()
            if(input==""||input.length<=3){
                input_tournament_title.error = "At least three characters!"
            } else{
                tc.addTournament(input,currentUser);
                (activity as MainActivity).showSpinner()
            }



        }
    }


    override fun update(obs: Observable?, arg: Any?) {

        if (arg!=null && arg is Tournament){
            val tournament = arg

            Ids.currentAdminTournament = tournament.tournamentId
            val bundle = Bundle()
            bundle.putString("id", tournament.tournamentId)
            val tournamentsFragment = TournamentDashboardFragment()
            tournamentsFragment.arguments = bundle
            (activity as MainActivity).hideSpinner()

            fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.flContent, tournamentsFragment).commit()
        }

        else if (arg!=null && arg is Exception){
            Log.d("Something", "Wicked this ways cometh")
        }

        else{
            toast("User not signed in!")
        }


    }


}