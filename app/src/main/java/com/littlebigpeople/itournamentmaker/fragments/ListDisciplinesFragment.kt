package com.littlebigpeople.itournamentmaker.fragments

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.adapters.DisciplinesAdapter
import com.littlebigpeople.itournamentmaker.data.Discipline
import com.littlebigpeople.itournamentmaker.state.TOURNAMENT_ID
import com.littlebigpeople.itournamentmaker.ui.DividerItemDecoration
import com.littlebigpeople.itournamentmaker.viewholders.DisciplineViewHolder
import kotlinx.android.synthetic.main.fragment_list_disciplines.*

/**
 * Created by dennisschmock on 11/12/2017.
 */
class ListDisciplinesFragment : Fragment() {

    var mAdapter: FirestoreRecyclerAdapter<Discipline, DisciplineViewHolder>? = null
    var tournamentId = ""
    val fb = FirebaseFirestore.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        tournamentId = this.arguments.getString(TOURNAMENT_ID)

        return inflater.inflate(R.layout.fragment_list_disciplines, container, false)


    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.title = "Disciplines"
        (activity as MainActivity).enableViews(true)

        btn_add_disciplines.setOnClickListener {
            val addEditFragment = AddEditDisciplineFragment()
            val args = Bundle()
            args.putString("id", tournamentId)
            addEditFragment.arguments = args
            fragmentManager.beginTransaction().replace(R.id.flContent, addEditFragment).addToBackStack("listdisc").commit()
        }

        val query: Query = FirebaseFirestore.getInstance().collection("Tournaments").document(tournamentId).collection("Disciplines")
        val options = FirestoreRecyclerOptions.Builder<Discipline>().setQuery(query, Discipline::class.java).build()

        setupRecyclerView(options)


    }

    fun setupRecyclerView(options: FirestoreRecyclerOptions<Discipline>) {
        mAdapter = DisciplinesAdapter(options, this,tournamentId)
        val itemDecorator = DividerItemDecoration(this.activity, DividerItemDecoration.VERTICAL_LIST)

        rv_disciplines.setHasFixedSize(true)
        rv_disciplines.layoutManager = LinearLayoutManager(this.activity)
        rv_disciplines.adapter = mAdapter
        rv_disciplines.addItemDecoration(itemDecorator)

        (mAdapter as DisciplinesAdapter).startListening()


    }

    override fun onStart() {
        super.onStart()
        if (mAdapter != null) {
            (mAdapter as DisciplinesAdapter).startListening()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mAdapter != null) {
            (mAdapter as DisciplinesAdapter).stopListening()
        }
    }

    override fun onResume() {
        super.onResume()



    }
}