package com.littlebigpeople.itournamentmaker.fragments

import android.app.AlertDialog
import android.app.Fragment
import android.app.FragmentManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.data.Discipline
import com.littlebigpeople.itournamentmaker.hideSoftKeyboard
import kotlinx.android.synthetic.main.fragment_add_edit_discipline.*
import org.jetbrains.anko.toast
import java.util.*

/**
 * Created by dennisschmock on 12/12/2017.
 */
class AddEditDisciplineFragment : Fragment() {

    var tournamentId = ""
    val fb = FirebaseFirestore.getInstance()
    var disciplineId = ""
    lateinit var discipline: Discipline

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        if (this.arguments != null) {
            tournamentId = this.arguments.getString("id")
            if (this.arguments.getString("disciplineid") != null) {
                disciplineId = this.arguments.getString("disciplineid")
            }
        }

        return inflater.inflate(R.layout.fragment_add_edit_discipline, container, false)

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.title = "Add/Edit discipline"

        if (disciplineId == "") {
            btn_delete_discipline.isEnabled = false


        } else {

            fb.collection("Tournaments").document(tournamentId).collection("Disciplines").document(disciplineId).get()
                    .addOnSuccessListener(object : OnSuccessListener<DocumentSnapshot> {
                        override fun onSuccess(p0: DocumentSnapshot?) {
                            if (p0!!.exists()) {
                                discipline = p0.toObject(Discipline::class.java)
                                toast("name" + discipline.name)
                                discipline_name.setText(discipline.name, TextView.BufferType.EDITABLE)
                            }
                        }
                    })
        }

        btn_cancel_discline.setOnClickListener {
            fragmentManager.popBackStack("listdisc", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }

        btn_delete_discipline.setOnClickListener {
            val deleteDialog = AlertDialog.Builder(activity)
            deleteDialog.setCancelable(true)
            deleteDialog.setTitle("Delete Discipline")
            deleteDialog.setMessage("This cannot be undone")
            deleteDialog.setPositiveButton(getString(R.string.delete)) { p0, p1 ->
                fb.collection("Tournaments").document(tournamentId).collection("Disciplines").document(disciplineId)
                        .delete()
                        .addOnSuccessListener({
                            toast("""${discipline.name} is deleted""")
                            fragmentManager.popBackStack("listdisc", FragmentManager.POP_BACK_STACK_INCLUSIVE)

                        })
                        .addOnFailureListener({ e -> TODO("add log")})

            }
            deleteDialog.setNegativeButton(getString(R.string.cancel)){p0,p1->

            }

            deleteDialog.show()
        }

        btn_save_discipline.setOnClickListener {
            saveDiscipline()

        }

    }

    fun saveDiscipline() {

        if (this::discipline.isInitialized) {
            discipline.name = discipline_name.text.toString()
            discipline.description = discipline_description.text.toString()

            fb.collection("Tournaments").document(tournamentId).collection("Disciplines")
                    .document(disciplineId).set(discipline as Discipline)
                    .addOnSuccessListener {
                        hideSoftKeyboard(activity)
                        fragmentManager.popBackStack("listdisc", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                    }
        } else {

            var title = discipline_name.text.toString()
            val description = discipline_description.text.toString()

            if (title.length <= 2) {
                discipline_name.error = getString(R.string.discipline_validation_error)
            } else {
                val pushkey = UUID.randomUUID().toString()

                (activity as MainActivity).showSpinner()
                title = title.trim()
                val discipline = Discipline(title, description, pushkey)

                fb.collection("Tournaments").document(tournamentId).collection("Disciplines").document(pushkey).set(discipline)
                        .addOnSuccessListener {

                            val listDisciplinesFragment = ListDisciplinesFragment()
                            val args = Bundle()
                            args.putString("id", tournamentId)
                            listDisciplinesFragment.arguments = args
                            hideSoftKeyboard(activity)
                            (activity as MainActivity).hideSpinner()

                            fragmentManager.popBackStack("listdisc", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                        }
            }
        }

    }

}