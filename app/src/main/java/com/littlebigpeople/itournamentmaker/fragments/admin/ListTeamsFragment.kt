package com.littlebigpeople.itournamentmaker.fragments.admin

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.adapters.TeamsAdapter
import com.littlebigpeople.itournamentmaker.data.Player
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.data.Tournament
import com.littlebigpeople.itournamentmaker.state.Ids
import com.littlebigpeople.itournamentmaker.ui.DividerItemDecoration
import com.littlebigpeople.itournamentmaker.util.FirestoreUtil
import com.littlebigpeople.itournamentmaker.util.TeamAssignerImpl
import com.littlebigpeople.itournamentmaker.viewholders.TeamViewHolder
import kotlinx.android.synthetic.main.fragment_list_teams.*
import org.jetbrains.anko.toast
import java.util.*

/**
 * Created by dennisschmock on 11/12/2017.
 */
class ListTeamsFragment : Fragment() {

    var mAdapter: FirestoreRecyclerAdapter<Team, TeamViewHolder>? = null
    var admin = false
    val fb = FirebaseFirestore.getInstance()
    var tournament: Tournament? = null
    var teamId = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        admin = this.arguments.getBoolean("isAdmin")

        (activity as MainActivity).enableViews(true)
        (activity as MainActivity).showSpinner()

        if (Ids.currentAdminTournament != null) {

            getTournament(Ids.currentAdminTournament as String)

        }
        return inflater.inflate(R.layout.fragment_list_teams, container, false)
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Ids.currentAdminTournament == null) return
        val query: Query = fb.collection("Tournaments")
                .document(Ids.currentAdminTournament as String).collection("Teams")
        val options = FirestoreRecyclerOptions.Builder<Team>().setQuery(query, Team::class.java).build()


        query.get().addOnSuccessListener { querySnapshot ->
            if (!querySnapshot.isEmpty) {
            btn_create_teams.visibility = View.GONE

            }
            (activity as MainActivity).hideSpinner()
        }.addOnFailureListener {
            (activity as MainActivity).hideSpinner()
            toast("Wrong creed!")
        }

        setupRecyclerViewOptions(options)

        btn_create_teams.setOnClickListener({
            if(tournament!=null&&(tournament as Tournament).playerCount<=1){
                toast("You need at least two players to create teams")
            } else{
                tournament?.let { it1 -> addTwoTeams(it1) }
                btn_create_teams.visibility = View.GONE
            }

        })

    }

    fun setupRecyclerViewOptions(options: FirestoreRecyclerOptions<Team>) {
        mAdapter = TeamsAdapter(options, this)
        val itemDecorator = DividerItemDecoration(this.activity, DividerItemDecoration.VERTICAL_LIST)
        rv_teams.layoutManager = LinearLayoutManager(this.activity)
        rv_teams.setHasFixedSize(true)
        rv_teams.adapter = mAdapter
        rv_teams.addItemDecoration(itemDecorator)

        (mAdapter as TeamsAdapter).startListening()
    }

    override fun onStart() {
        super.onStart()

        if (mAdapter != null) {
            (mAdapter as TeamsAdapter).startListening()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mAdapter != null) {
            (mAdapter as TeamsAdapter).stopListening()
        }
    }

    fun getTournament(tournamentId: String) {
        fb.collection("Tournaments").document(tournamentId).get()
                .addOnSuccessListener { p0 ->
                    if (p0 != null && p0.exists()) {
                        val tournament = p0.toObject(Tournament::class.java)
                        getTeams(tournament)

                    }
                }.addOnFailureListener {
            toast("Invalid tournament ID")
        }
    }
//TODO needs total rewrite!

    fun getTeams(tournament: Tournament) {
        fb.collection("Tournaments").document(tournament.tournamentId).collection("teams").get()
                .addOnSuccessListener { p0 ->
                    if (p0 != null) {
                        val teamDTOs: MutableList<Team> = p0.toObjects(Team::class.java)
                        val teams: MutableList<Team> = ArrayList<Team>()
                        for (team in teamDTOs) {
                            teams.add(team)
                        }
                    }
                }.addOnFailureListener {
            toast("Invalid tournament ID")
        }

    }

    fun addTwoTeams(tournament: Tournament) {
        var players = ArrayList<Player>()
        fb.collection("Tournaments").document(tournament.tournamentId).collection("Players").get()
                .addOnSuccessListener { p0 ->
                    if (p0 != null && tournament.teams.isEmpty()) {
                        val players = p0.toObjects(Player::class.java)
                        for (player in players) {
                            players.add(player)
                        }
                        val generateTeams = TeamAssignerImpl().generateTeams(players, 2, tournament.tournamentId)
                        FirestoreUtil().submitTeams(tournament, generateTeams)
                    }
                }
    }
}