package com.littlebigpeople.itournamentmaker.fragments.joined

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.adapters.MatchesAdapter
import com.littlebigpeople.itournamentmaker.data.Match
import com.littlebigpeople.itournamentmaker.data.MatchState
import com.littlebigpeople.itournamentmaker.state.Ids
import com.littlebigpeople.itournamentmaker.state.MATCHES
import com.littlebigpeople.itournamentmaker.state.PLAYED_MATCHES
import com.littlebigpeople.itournamentmaker.state.TOURNAMENTS
import com.littlebigpeople.itournamentmaker.ui.DividerItemDecoration
import com.littlebigpeople.itournamentmaker.viewholders.MatchViewHolder
import kotlinx.android.synthetic.main.fragment_list_matches.*

/**
 * Created by Dennis on 17-12-2017.
 */
class ListJoinedMatchesFragment : Fragment(){

    var mAdapter: FirestoreRecyclerAdapter<Match, MatchViewHolder>? = null
    var tournamentId = Ids.currentJoinedTournament
    val fs = FirebaseFirestore.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_list_matches, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val played = this.arguments.getBoolean(PLAYED_MATCHES)
        var state = ""
        if(played){
            state = MatchState.PLAYED.toString()
        } else{
            state = MatchState.READY.toString()
        }

        val query: Query = FirebaseFirestore.getInstance().collection(TOURNAMENTS).document(Ids.currentJoinedTournament!!).collection(MATCHES).whereEqualTo("state", state)
        val options = FirestoreRecyclerOptions.Builder<Match>().setQuery(query, Match::class.java).build()
        setupRecyclerView(options)
        btn_create_schedule.visibility = View.INVISIBLE
        (activity as MainActivity).enableViews(true)

    }

    fun setupRecyclerView(options: FirestoreRecyclerOptions<Match>){

        mAdapter = MatchesAdapter(options, this, tournamentId as String)
        val itemDecorator = DividerItemDecoration(this.activity, DividerItemDecoration.VERTICAL_LIST)

        rv_matches.layoutManager = LinearLayoutManager(this.activity)
        rv_matches.setHasFixedSize(true)
        rv_matches.adapter = mAdapter
        rv_matches.addItemDecoration(itemDecorator)
        if (mAdapter != null) {
            mAdapter?.startListening()
        }
    }


}