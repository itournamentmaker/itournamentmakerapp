package com.littlebigpeople.itournamentmaker.fragments.joined

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.components.TournamentComponent
import com.littlebigpeople.itournamentmaker.data.Tournament
import com.littlebigpeople.itournamentmaker.state.Ids
import kotlinx.android.synthetic.main.fragment_join_tournament.*
import org.jetbrains.anko.toast
import java.util.*

/**
 * Created by Dennis on 27-11-2017.
 */
class JoinTournamentFragment: Fragment(), Observer {


    val fb: FirebaseFirestore = FirebaseFirestore.getInstance()
    val tc = TournamentComponent()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        return inflater.inflate(R.layout.fragment_join_tournament,container,false)
    }

    override fun onResume() {
        super.onResume()
        tc.addObserver(this)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.title = getString(R.string.join_tournament)

        btn_join_tournament.setOnClickListener {
            joinTournament();
        }
    }

    fun joinTournament(){
        val tournamentId = edittext_join.text.toString()
        if (tournamentId.length == 6) {
            (activity as MainActivity).showSpinner()

            tc.getTournament(tournamentId)
        }

        else{
            Toast.makeText(activity, "The code should be 6 characters long!",Toast.LENGTH_LONG).show() // todo. Remember to change to a better notification
        }
    }
    override fun update(obs: Observable?, arg: Any?) {
        if (arg is Tournament) {
            Ids.setCurentJoinedTournament(activity, arg.tournamentId)
            val joinedTournamentFragment = JoinedTournamentFragment()

            (activity as MainActivity).hideSpinner()

            activity.fragmentManager.beginTransaction().replace(R.id.flContent, joinedTournamentFragment).commit()
        }
        else if (arg is Exception){
            toast("Invalid tournament ID")
        }
    }

    override fun onStop() {
        super.onStop()
        tc.deleteObserver(this)
    }
}