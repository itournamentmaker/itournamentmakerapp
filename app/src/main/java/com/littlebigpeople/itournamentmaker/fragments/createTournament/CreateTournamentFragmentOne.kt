package com.littlebigpeople.itournamentmaker.fragments.createTournament

import android.app.Fragment
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.activities.CreateTournament
import com.littlebigpeople.itournamentmaker.data.TournamentType
import com.littlebigpeople.itournamentmaker.viewmodels.TournamentVM
import kotlinx.android.synthetic.main.fragment_create_tournament_one.*


/**
 * Created by Dennis on 27-11-2017.
 */
class CreateTournamentFragmentOne : Fragment(), AdapterView.OnItemSelectedListener {


    val fb: FirebaseFirestore = FirebaseFirestore.getInstance()
    val currentUser = FirebaseAuth.getInstance().currentUser
    lateinit var descriptions: Array<String>
    var selector = 0

    private val tournamentViewModel by lazy {
        ViewModelProviders.of(activity as CreateTournament).get(TournamentVM::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_create_tournament_one, container, false)
    }

    override fun onResume() {
        super.onResume()
        spinner.setSelection(tournamentViewModel.selector)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        input_tournament_title.setText(tournamentViewModel.tournament.name)
        setupButtonListeners()


        descriptions = resources.getStringArray(R.array.tournament_types_description)


        //Setup selection spinner
        val adapter = ArrayAdapter.createFromResource(activity, R.array.tournament_types, R.layout.item_spinner_test)
        adapter.setDropDownViewResource(R.layout.item_spinner_test)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = this

        stepProgressBar.steps.add("Name/Type")
        stepProgressBar.steps.add("Disciplines")
        stepProgressBar.steps.add("Settings")
        stepProgressBar.setCurrentStep(1)

        //hide keyboard on enter. Redundant from activity, but necessary.

    }


    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, p3: Long) {

        selector = position
        tournamentViewModel.selector = position

        text_tournament_type.text = descriptions[position]


    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    private fun setupButtonListeners() {
        btn_cancel.setOnClickListener {
            activity.finish()
        }


        input_tournament_title.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_ENTER) {

                (activity as CreateTournament).hideSoftKeyboard(activity)
                input_tournament_title.isFocusable = false
                input_tournament_title.isFocusableInTouchMode = true
                true
            } else {
                false
            }
        }

        btn_next_step.setOnClickListener {

            //Validate input todo: refactor
            val input = input_tournament_title.text.toString().trim()
            if (input == "" || input.length < 3) {
                input_tournament_title.error = getString(com.littlebigpeople.itournamentmaker.R.string.input_error)
            } else if (selector == 0) {
                (spinner.selectedView as TextView).error = getString(com.littlebigpeople.itournamentmaker.R.string.pick_tournament_type)
            } else {
                tournamentViewModel.tournament.name = input
                val tourType = TournamentType.valueOf(selector)
                if (tourType != null) tournamentViewModel.tournament.tournamentType = tourType

                val fragmentTwo = CreateTournamentFragmentTwo()
                (activity as CreateTournament).navigateToFragment(fragmentTwo)
            }
        }
    }




}