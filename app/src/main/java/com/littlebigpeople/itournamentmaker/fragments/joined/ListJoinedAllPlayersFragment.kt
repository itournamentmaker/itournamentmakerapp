package com.littlebigpeople.itournamentmaker.fragments.joined

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.adapters.PlayersAdapter
import com.littlebigpeople.itournamentmaker.data.Player
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.state.Ids
import com.littlebigpeople.itournamentmaker.state.PLAYERS
import com.littlebigpeople.itournamentmaker.state.TOURNAMENTS
import com.littlebigpeople.itournamentmaker.ui.DividerItemDecoration
import com.littlebigpeople.itournamentmaker.viewholders.PlayerViewHolder
import kotlinx.android.synthetic.main.fragment_list_joined_all_players.*

/**
 * Created by dennisschmock on 08/12/2017.
 */
class ListJoinedAllPlayersFragment : Fragment() {
    var mAdapter: FirestoreRecyclerAdapter<Player, PlayerViewHolder>? = null
    //var tournamentId: String? = null
    var teamUuid: String? = null
    var team: Team? = null
    val fb = FirebaseFirestore.getInstance()
    lateinit var tournamentRef: DocumentReference

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        tournamentRef = FirebaseFirestore.getInstance().collection(TOURNAMENTS).document(Ids.currentJoinedTournament!!)
        return inflater.inflate(R.layout.fragment_list_joined_all_players, container, false)

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).enableViews(true)


        if(Ids.currentJoinedTournament != null ) {
            val query: Query = tournamentRef.collection(PLAYERS)
            val options = FirestoreRecyclerOptions.Builder<Player>().setQuery(query, Player::class.java).build()
            setupRecyclerView(options)
        }

    }

    fun setupRecyclerView(options: FirestoreRecyclerOptions<Player>) {
        mAdapter = PlayersAdapter(options, this)
        val itemDecorator = DividerItemDecoration(this.activity, DividerItemDecoration.VERTICAL_LIST)

        rv_joined_all_players.toString()
        rv_joined_all_players.layoutManager = LinearLayoutManager(this.activity)
        rv_joined_all_players.setHasFixedSize(true)
        rv_joined_all_players.adapter = mAdapter
        rv_joined_all_players.addItemDecoration(itemDecorator)
        if (mAdapter != null) {
            mAdapter?.startListening()
        }
    }

}