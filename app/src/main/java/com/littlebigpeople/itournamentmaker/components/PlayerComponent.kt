package com.littlebigpeople.itournamentmaker.components

import com.google.firebase.firestore.FirebaseFirestore
import com.littlebigpeople.itournamentmaker.data.Player
import com.littlebigpeople.itournamentmaker.state.PLAYERS
import com.littlebigpeople.itournamentmaker.state.TOURNAMENTS
import java.util.*


class PlayerComponent: Observable(){
    val fb = FirebaseFirestore.getInstance()

    fun addPlayerToTournament(tournamentId: String, player: Player){

        fb.collection(TOURNAMENTS).document(tournamentId).collection(PLAYERS).document(player.id).set(Player(player.id,player.name,player.teamName,player.teamUuid))
                .addOnSuccessListener {
                    setChanged()
                    notifyObservers(player)
                }
    }

    fun getPlayer(tournamentId: String,playerId: String){

        fb.collection(TOURNAMENTS).document(tournamentId).collection(PLAYERS).document(playerId).get()
                .addOnSuccessListener { p0 ->
                    if(p0!=null && p0.exists()) {
                        val player = p0.toObject(Player::class.java)
                        setChanged()
                        notifyObservers(player)
                    }
                }
    }
}