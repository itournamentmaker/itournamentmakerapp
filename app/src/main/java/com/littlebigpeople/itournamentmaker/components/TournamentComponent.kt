package com.littlebigpeople.itournamentmaker.components

import android.util.Log
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Transaction
import com.littlebigpeople.itournamentmaker.data.*
import com.littlebigpeople.itournamentmaker.state.DISCIPLINES
import com.littlebigpeople.itournamentmaker.state.TOURNAMENTS
import java.util.*
import kotlin.collections.ArrayList

class TournamentComponent : Observable() {

    val fb: FirebaseFirestore = FirebaseFirestore.getInstance()


    fun createTournament(tournament: Tournament, disciplines: ArrayList<Discipline>, finished: () -> Unit) {
        val uuid = UUID.randomUUID().toString().subSequence(0, 6)
        tournament.tournamentId = uuid.toString()
        val reference: DocumentReference = fb.collection(TOURNAMENTS).document(uuid.toString())

        val batch = fb.batch()

        batch.set(reference, tournament)


        for (discipline in disciplines) {
            val disUUID = UUID.randomUUID().toString()
            val disReference: DocumentReference = fb.collection(TOURNAMENTS).document(uuid.toString()).collection(DISCIPLINES).document(disUUID)
            discipline.uuid = disUUID
            batch.set(disReference, discipline)
        }

        batch.commit().addOnCompleteListener {
            finished()
        }.addOnFailureListener {
            Log.e("DataBase", it.message)
            finished()
        }
    }


    fun addTournament(title: String, currentUser: FirebaseUser?) {

        //Build tournament object
        val tournamentName = title

        val teams = mutableListOf<Team>()
        val disciplines = mutableListOf<Discipline>()
        val matches = mutableListOf<Match>()
        val uuid = UUID.randomUUID().toString().subSequence(0, 6) //Beware the collision risk!
        Log.d("time", uuid.toString())

        //if currentuser is signed in, create tournament
        if (currentUser != null) {
            val tournamentUser = currentUser.uid
            var tournamentUserName = "no name"
            if (currentUser.displayName != null) {
                tournamentUserName = currentUser.displayName as String
            }


            val tournament = Tournament(tournamentName, uuid.toString(), tournamentUser, tournamentUserName, 0, TournamentState.PREPARE)

            fb.collection(TOURNAMENTS).document(uuid.toString()).set(tournament)
                    .addOnSuccessListener {
                        setChanged()
                        notifyObservers(tournament)
                    }
                    .addOnFailureListener { p0 ->
                        setChanged()
                        notifyObservers(p0)
                    }

        } else {
            setChanged()
            notifyObservers()
        }
    }

    fun updateTournamentState(state: TournamentState, tournamentId: String) {
        fb.collection(TOURNAMENTS).document(tournamentId).update("tournamentState", state.toString())
    }

    fun getTournament(tournamentId: String) {
        fb.collection(TOURNAMENTS).document(tournamentId).get()
                .addOnSuccessListener { p0 ->
                    if (p0 != null && p0.exists()) {
                        val tournament = p0.toObject(Tournament::class.java)
                        setChanged()
                        notifyObservers(tournament)
                    }
                }.addOnFailureListener { p0 ->
                    setChanged()
                    notifyObservers(p0)
                }

    }

}