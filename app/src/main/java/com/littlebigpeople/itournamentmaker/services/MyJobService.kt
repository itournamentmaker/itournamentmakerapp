package com.littlebigpeople.itournamentmaker.services

import android.app.job.JobParameters
import android.app.job.JobService
import android.util.Log

/**
 * Created by Dennis on 30-12-2017.
 */
class MyJobService : JobService() {
    private val TAG = "MyJobService"


    override fun onStopJob(p0: JobParameters?): Boolean {
        Log.d(TAG,"Performing long running task in job")
        return false
    }

    override fun onStartJob(p0: JobParameters?): Boolean {
        return false
    }
}