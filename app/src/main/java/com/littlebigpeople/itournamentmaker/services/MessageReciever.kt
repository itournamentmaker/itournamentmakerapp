package com.littlebigpeople.itournamentmaker.services

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.firebase.jobdispatcher.GooglePlayDriver
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.littlebigpeople.itournamentmaker.MainActivity
import com.littlebigpeople.itournamentmaker.R


/**
 * Created by Dennis on 30-12-2017.
 */
class MessageReciever: FirebaseMessagingService() {

    private val TAG = "MyFireBaseService"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.from)

        if(remoteMessage.data.isNotEmpty()){
            Log.d(TAG, "Message data payload: "+ remoteMessage.data)
            if(true){
                scheduleJob();
            } else{
                handleNow();
            }

            if (remoteMessage.notification !=null){
                Log.d(TAG, "Message notification body: " + remoteMessage.notification.body)
            }

            sendNotification(remoteMessage.notification.body)
        }


    }
    private fun handleNow(){
        Log.d(TAG, "Short lived task is done")
    }

    private fun sendNotification (messageBody: String?){
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this,channelId)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("Message from iTournamentMaker")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(0,notificationBuilder.build())
    }

    private fun scheduleJob() {
        // [START dispatch_job]
        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(this))
        val myJob = dispatcher.newJobBuilder()
                .setService(MyJobSer::class.java)
                .setTag("my-job-tag")
                .build()
        dispatcher.schedule(myJob)
        // [END dispatch_job]
    }
}