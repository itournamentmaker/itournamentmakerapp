package com.littlebigpeople.itournamentmaker.viewholders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.littlebigpeople.itournamentmaker.R

/**
 * Created by dennisschmock on 01/12/2017.
 */
class TournamentViewHolder(view:View): RecyclerView.ViewHolder(view) {
    val title : TextView = view.findViewById(R.id.list_tournament_title)
    val description : TextView = view.findViewById(R.id.list_tournament_desc)

}