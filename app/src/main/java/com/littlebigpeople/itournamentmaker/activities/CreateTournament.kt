package com.littlebigpeople.itournamentmaker.activities

import android.app.Activity
import android.app.Fragment
import android.app.FragmentTransaction
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.fragments.createTournament.CreateTournamentFragmentOne
import com.littlebigpeople.itournamentmaker.viewmodels.TournamentVM
import kotlinx.android.synthetic.main.activity_create_tournament.*


class CreateTournament : AppCompatActivity() {
    private val tournamentViewModel by lazy {
        ViewModelProviders.of(this).get(TournamentVM::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_tournament)

        setFragment()
    }


    private fun setFragment() {
        val fragment = CreateTournamentFragmentOne()
        fragmentManager.beginTransaction().replace(R.id.createTournamentFrame, fragment).commit()
    }

    fun showSpinner() {
        catch_touch_layer.visibility = View.VISIBLE
        loading_spinner.visibility = View.VISIBLE

    }

    fun hideSpinner() {
        catch_touch_layer.visibility = View.GONE
        loading_spinner.visibility = View.GONE
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.windowToken, 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    fun navigateToFragment(fragment: Fragment) {


        val ft: FragmentTransaction = fragmentManager.beginTransaction()
        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)

        ft.replace(R.id.createTournamentFrame, fragment).addToBackStack(null).commit()
    }

    fun hideSoftKeyboard(activity: Activity) {
        if (activity.currentFocus == null) {
            return
        }
        val inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
    }
}
