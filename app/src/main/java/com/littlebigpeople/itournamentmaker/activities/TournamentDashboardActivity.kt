package com.littlebigpeople.itournamentmaker.activities

import android.app.Activity
import android.app.AlertDialog
import android.app.Fragment
import android.app.FragmentTransaction
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import com.google.firebase.firestore.*
import com.littlebigpeople.itournamentmaker.R
import com.littlebigpeople.itournamentmaker.components.TournamentComponent
import com.littlebigpeople.itournamentmaker.data.Tournament
import com.littlebigpeople.itournamentmaker.data.TournamentState
import com.littlebigpeople.itournamentmaker.fragments.admin.ListMatchesFragment
import com.littlebigpeople.itournamentmaker.fragments.admin.ListPlayersFragment
import com.littlebigpeople.itournamentmaker.fragments.admin.ListTeamsFragment
import com.littlebigpeople.itournamentmaker.state.PLAYED_MATCHES
import com.littlebigpeople.itournamentmaker.state.TOURNAMENT_ID
import kotlinx.android.synthetic.main.fragment_dashboard_tournament.*
import kotlinx.android.synthetic.main.toolbar.*

/**
 * Created by Dennis on 06-12-2017.
 */
class TournamentDashBoardActivity : Activity(), EventListener<DocumentSnapshot> {

    private var mTournamentRegistration: ListenerRegistration? = null
    private var mTournamentRef: DocumentReference? = null
    private var tournamentId = ""
    private val db = FirebaseFirestore.getInstance()
    private val tc = TournamentComponent()
    private var state = TournamentState.PROGRESS


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      //  setContentView(R.layout.activity_d)

    }

    private fun setupButtons() {
        btn_players_list.setOnClickListener {

            val fragment = ListPlayersFragment()
            navigateToFragment(null, fragment)

        }

        btn_teams_list.setOnClickListener {

            val fragment = ListTeamsFragment()
            navigateToFragment(null, fragment)

        }

        btn_disciplines.setOnClickListener {

         //   val fragment = ListDisciplinesFragment()
         //   navigateToFragment(null, fragment)


        }

        btn_list_played_matches.setOnClickListener {
            val fragment = ListMatchesFragment()

            navigateToFragment(true, fragment)

        }

        btn_list_matches.setOnClickListener {

            val fragment = ListMatchesFragment()
            navigateToFragment(false, fragment)


        }

        btn_scoreboard.setOnClickListener {

           // val fragment = ListScoresFragment()
         //   navigateToFragment(false, fragment)

        }

        btn_tournament_start.setOnClickListener {
            startTournamentDialogBuilder()

        }


    }


    private fun navigateToFragment(playedMatches: Boolean?, fragment: Fragment) {
        val bundle = Bundle()
        bundle.putString(TOURNAMENT_ID, tournamentId)
        if (playedMatches != null) {
            bundle.putBoolean(PLAYED_MATCHES, playedMatches)
        }
        fragment.arguments = bundle

        val ft: FragmentTransaction = fragmentManager.beginTransaction()
        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)

        ft.replace(R.id.flContent, fragment).addToBackStack(null).commit()
    }


    override fun onStart() {
        super.onStart()
        mTournamentRegistration = mTournamentRef?.addSnapshotListener(this)
    }

    override fun onEvent(snapshot: DocumentSnapshot?, e: FirebaseFirestoreException?) {

        if (e != null) {
            Log.d("FS Exception", e.toString())
            return
        }
        if (snapshot != null && snapshot.exists()) {
            onTournamentLoaded(snapshot?.toObject(Tournament::class.java))

        }

    }

    private fun onTournamentLoaded(tournament: Tournament?) {
        if (tournament != null) {
            state = tournament.tournamentState
            text_players_number.text = """${tournament.playerCount} players"""
            text_join_id.text = "${getString(R.string.tournament_id)}${tournament.tournamentId}"
            this.toolbar.title = tournament.name
            text_state.text = "State: " + tournament.tournamentState.toString()
            when(state){
                TournamentState.PREPARE-> btn_tournament_start.text = "Start Tournament"
                TournamentState.PROGRESS-> btn_tournament_start.text = "Finish Tournament"
                TournamentState.FINISHED -> {
                    btn_tournament_start.isEnabled = false
                    btn_tournament_start.text = "Tournament Finished"
                    btn_tournament_start.setBackgroundColor(Color.DKGRAY)
                }
            }

        }

    }

    override fun onStop() {
        super.onStop()
        if (mTournamentRegistration != null) {
            //smartcasting not working here?
            mTournamentRegistration!!.remove()
            mTournamentRegistration = null
        }
    }

    private fun startTournamentDialogBuilder() {

        val alertDialog = AlertDialog.Builder(this)
                .setCancelable(true)
                .setTitle(getString(R.string.start_tournament_title))
                .setMessage(getString(R.string.start_tournament_message))
                .setPositiveButton(getString(R.string.yes)) { p0, p1 ->
                    when(state) {
                        TournamentState.PREPARE -> tc.updateTournamentState(TournamentState.PROGRESS, tournamentId)
                        TournamentState.PROGRESS -> tc.updateTournamentState(TournamentState.FINISHED,tournamentId)
                    }

                }
                .setNegativeButton(R.string.cancel) { p0, _ ->
                    p0.cancel()
                }.create()

        alertDialog.show()

    }




}