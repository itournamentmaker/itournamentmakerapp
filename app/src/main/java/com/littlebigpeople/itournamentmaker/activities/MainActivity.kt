package com.littlebigpeople.itournamentmaker

import android.animation.ValueAnimator
import android.app.*
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import com.littlebigpeople.itournamentmaker.fragments.ListTournamentsFragment
import com.littlebigpeople.itournamentmaker.fragments.MainFragment
import com.littlebigpeople.itournamentmaker.fragments.admin.CreateTournamentFragment
import com.littlebigpeople.itournamentmaker.fragments.joined.JoinTournamentFragment
import com.littlebigpeople.itournamentmaker.state.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header.view.*
import kotlinx.android.synthetic.main.toolbar.*


class MainActivity : AppCompatActivity() {

    private val RC_SIGN_IN = 123;

    lateinit var auth: FirebaseAuth
    lateinit var menu: Menu
    lateinit var mDrawerToggle: ActionBarDrawerToggle
    private var mToolBarNavigationListenerIsRegistered = false


    lateinit var currentPlayerId: String
    lateinit var currentTournamentId: String

    var headerView: View? = null
    private lateinit var authStateListener: FirebaseAuth.AuthStateListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth = FirebaseAuth.getInstance()

        setSupportActionBar(toolbar)

        mDrawerToggle = setupDrawerToggle()
        mDrawerToggle.syncState();

        drawer_layout.addDrawerListener(mDrawerToggle)

        //Get saved settings
        val settings = getSharedPreferences(PREFS_NAME, 0)
        currentPlayerId = settings.getString(PLAYER_ID, "")
        currentTournamentId = settings.getString(TOURNAMENT_ID, "")

        setupDrawerContent(nvView)
        setMainFragment()
        setupAuthStateListener()
        restorePrefs()

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item == null) return false
        when (item.itemId) {
            R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        auth.addAuthStateListener(authStateListener)
    }

    override fun onStop() {
        super.onStop()
        auth.removeAuthStateListener(authStateListener)
    }

    private fun setupDrawerContent(navigationView: NavigationView) {
        navigationView.setNavigationItemSelectedListener { item ->
            selectDrawerItem(item)
            true
        }

    }

    fun selectDrawerItem(menuItem: MenuItem) {

        when (menuItem.itemId) {
            R.id.nav_create_tournament -> {
                val fragment = CreateTournamentFragment()
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit()
            }
            R.id.nav_my_tournaments -> {
                val fragment = ListTournamentsFragment()
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit()
            }
            R.id.nav_join_tournament -> {
                val fragment = JoinTournamentFragment()
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit()
            }
            R.id.nav_logout -> {
                logout()
            }
            R.id.nav_login -> {
                login()
            }
            else -> {
                val fragment = MainFragment()
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit()
            }
        }

        menuItem.setChecked(true)
        setTitle(menuItem.title)
        drawer_layout.closeDrawers()
    }


    private fun setupDrawerToggle(): ActionBarDrawerToggle {
        return ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.drawer_open, R.string.drawer_close)
    }

    private fun setMainFragment() {
        val bundle = Bundle()
        bundle.putString(PLAYER_ID, currentPlayerId)
        bundle.putString(TOURNAMENT_ID, currentTournamentId)
        val fragment = MainFragment()
        fragment.arguments = bundle
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit()
    }

    companion object {
        fun navigateToFragment(context: Fragment, fragment: Fragment, tournamentId: String, shouldStackPop: Boolean, name: String?, shouldPopTo: String?) {
            val bundle = Bundle()
            bundle.putString("id", tournamentId)
            fragment.arguments = bundle

            if (shouldStackPop) {
                context.fragmentManager.popBackStack(shouldPopTo, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }
            context.fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(name).commit()
        }
    }

    private fun login() {

        if (auth.currentUser == null) {
            val providers = mutableListOf(
                    AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
                    AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build()
            )
            startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().setAvailableProviders(providers).build(), RC_SIGN_IN)
        }
    }


    private fun logout() {
        AuthUI.getInstance().signOut(this).addOnCompleteListener {
            val snackBar = Snackbar.make(coordinator_layout, "User Logged out!", Snackbar.LENGTH_LONG)
            snackBar.show()
        }
    }


    private fun setupAuthStateListener() {
        authStateListener = object : FirebaseAuth.AuthStateListener {
            override fun onAuthStateChanged(p0: FirebaseAuth) {
                val firebaseUser = auth.currentUser
                headerView = nvView.getHeaderView(0)


                if (firebaseUser != null) {
                    nvView.menu.findItem(R.id.nav_my_tournaments).isVisible = true
                    nvView.menu.findItem(R.id.nav_create_tournament).isVisible = true
                    nvView.menu.findItem(R.id.submenu_settings).isVisible = true
                    nvView.menu.findItem(R.id.nav_login).isVisible = false
                    nvView.menu.findItem(R.id.nav_logout).isVisible = true

                    headerView!!.header_account_name.text = firebaseUser.email
                    headerView!!.header_accoutn_email.text = firebaseUser.displayName


                } else {
                    nvView.menu.findItem(R.id.nav_login).isVisible = true
                    nvView.menu.findItem(R.id.nav_logout).isVisible = false

                    nvView.menu.findItem(R.id.nav_my_tournaments).isVisible = false
                    nvView.menu.findItem(R.id.nav_create_tournament).isVisible = false
                    nvView.menu.findItem(R.id.submenu_settings).isVisible = false
                    headerView!!.header_account_name.text = getString(R.string.anonymous_user)
                    headerView!!.header_accoutn_email.text = ""
                    setMainFragment()

                }

            }
        }
    }

    fun restorePrefs() {
        val storePlayer: SharedPreferences = getSharedPreferences(PREFS_NAME, 0)
        Ids.currentJoinedTournament = storePlayer.getString(TOURNAMENT_ID, "")
        Ids.currentJoinedTeam = storePlayer.getString(TEAM_ID, "")
        Ids.currentJoinedPlayer = storePlayer.getString(PLAYER_ID, "")
    }

    fun showSpinner() {
        catch_touch_layer.visibility = View.VISIBLE
        loading_spinner.visibility = View.VISIBLE

    }

    fun hideSpinner() {
        catch_touch_layer.visibility = View.GONE
        loading_spinner.visibility = View.GONE
    }

    fun enableViews(enable: Boolean) {
        var anim: ValueAnimator? = null

        if (enable) {

            mDrawerToggle.isDrawerIndicatorEnabled = false

            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            if (!mToolBarNavigationListenerIsRegistered) {
                mDrawerToggle.toolbarNavigationClickListener = View.OnClickListener {
                    // Doesn't have to be onBackPressed
                    onBackPressed()
                }

                mToolBarNavigationListenerIsRegistered = true
            }
        } else {
            anim = ValueAnimator.ofFloat(1F, 0F)

            // Remove back button
            supportActionBar?.setDisplayHomeAsUpEnabled(false)

            // Show hamburger
            mDrawerToggle.isDrawerIndicatorEnabled = true

            // Remove the/any drawer toggle listener
            mDrawerToggle.toolbarNavigationClickListener = null
            mToolBarNavigationListenerIsRegistered = false;

        }


    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setupMessagingService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            val channelId = getString(R.string.default_notification_channel_id)
            val channelName = getString(R.string.default_notification_channel_name)
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager!!.createNotificationChannel(NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW))
        }
        FirebaseMessaging.getInstance().subscribeToTopic("News")

    }
}


fun hideSoftKeyboard(activity: Activity) {
    if (activity.currentFocus == null) {
        return
    }
    val inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
}