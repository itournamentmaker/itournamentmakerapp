package com.littlebigpeople.itournamentmaker.state

/**
 * Created by dennisschmock on 15/12/2017.
 */
val BACK_STACK_ROOT_TAG = "root_fragment"
val PREFS_NAME = "LocalPlayerStorage"
val PLAYER_ID = "currentPlayer"
val TEAM_ID = "currentTeam"
val TOURNAMENT_ID = "currentTournament"
val MATCH_ID = "matchId"
val PLAYED_MATCHES = "played"

//Database names
val TOURNAMENTS = "Tournaments"
val DISCIPLINES = "Disciplines"
val MATCHES = "Matches"
val PLAYERS = "Players"
val TEAMS = "Teams"