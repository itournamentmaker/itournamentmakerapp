package com.littlebigpeople.itournamentmaker.state

import android.content.Context
import android.content.SharedPreferences
import com.google.firebase.firestore.DocumentReference

/**
 * Created by daniel on 12/19/17.
 */
object Ids {

    var currentJoinedTournament: String? = null
    var currentJoinedTeam: String? = null
    var currentJoinedPlayer: String? = null

    var currentAdminTournament: String? = null


    var currentJoinedTournamentReference: DocumentReference? = null



    fun setCurentJoinedTournament(context: Context, tournamentId: String){ // If context is not saved, we're not leaking (?)
        currentJoinedTournament = tournamentId
        val storePlayer: SharedPreferences = context.getSharedPreferences(PREFS_NAME, 0)
        val editor = storePlayer.edit()
        editor.putString(TOURNAMENT_ID, tournamentId)

        editor.commit()
    }

    fun setCurentJoinedTeam(context: Context, teamId: String){ // If context is not saved, we're not leaking (?)
        currentJoinedTeam = teamId
        val storePrefs: SharedPreferences = context.getSharedPreferences(PREFS_NAME, 0)
        val editor = storePrefs.edit()
        editor.putString(TEAM_ID, teamId)
        editor.commit()
    }

    fun setCurentJoinedPlayer(context: Context, playerId: String){ // If context is not saved, we're not leaking (?)
        currentJoinedPlayer = playerId
        val storePlayer: SharedPreferences = context.getSharedPreferences(PREFS_NAME, 0)
        val editor = storePlayer.edit()
        editor.putString(PLAYER_ID, playerId)
        editor.commit()
    }







}