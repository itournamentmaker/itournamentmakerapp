package com.littlebigpeople.itournamentmaker

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.replaceText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.DrawerActions
import android.support.test.espresso.contrib.NavigationViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.littlebigpeople.itournamentmaker.data.TournamentState
import com.littlebigpeople.itournamentmaker.state.PLAYERS
import com.littlebigpeople.itournamentmaker.state.TOURNAMENTS
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import java.util.*
import java.util.concurrent.CountDownLatch


/**
 * Created by Dennis on 24-12-2017.
 */
open class UserNavTest {

    val fb = FirebaseFirestore.getInstance()
    var authSignal: CountDownLatch? = null
    var firebaseUser: FirebaseUser? = null
    lateinit var auth: FirebaseAuth
   // val tournamentId = UUID.randomUUID()
   // val userName = "TESTUSER"

    @Rule
    @JvmField
    public var activityTestRule = ActivityTestRule<MainActivity>(MainActivity::class.java)

    companion object {
        val fb = FirebaseFirestore.getInstance()
        val tournamentId = UUID.randomUUID().toString().subSequence(0, 6).toString()
        val userName = "TESTUSER"
        init {

        }

        @BeforeClass @JvmStatic
        fun setup() {
            val tournamentDetails = TournamentDetailsDTO("TEST", "test")
            val tournament = TournamentDTO(tournamentDetails, tournamentId, "test", 0, TournamentState.PREPARE)
            fb.collection(TOURNAMENTS).document(tournamentId).set(tournament)


        }

        @AfterClass @JvmStatic
        fun teardown() {
            val userId = fb.collection(TOURNAMENTS).document(tournamentId).collection(PLAYERS).whereEqualTo("name", userName).get().addOnSuccessListener { querySnapshot ->
                querySnapshot.forEach { snap ->
                    val player = snap.toObject(PlayerDTO::class.java)
                    fb.collection(TOURNAMENTS).document(tournamentId).collection(PLAYERS).document(player.id).delete()
                }
            }
            fb.collection(TOURNAMENTS).document(tournamentId).delete()

        }

    }



    @Test
    fun joinTournamentTest() {

        onView(ViewMatchers.withId(R.id.drawer_layout)).perform(DrawerActions.open())

        onView(ViewMatchers.withId(R.id.nvView))
                .perform((NavigationViewActions.navigateTo((R.id.nav_join_tournament))))

        onView(withId(R.id.edittext_join)).perform(replaceText(tournamentId))
        onView(withId(R.id.btn_join_tournament)).perform(click())

        onView(withId(R.id.edittext_enter_name)).perform(replaceText(userName))
        onView(withId(R.id.btn_submit_name)).perform(click())

        //ugly - todo implement espresso idle
        //Thread.sleep(500)
        onView(withId(R.id.val_player_name)).check(matches(withText(userName)))

    }


}






