package com.littlebigpeople.itournamentmaker

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.DrawerActions
import android.support.test.espresso.contrib.DrawerMatchers
import android.support.test.espresso.contrib.NavigationViewActions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.view.Gravity
import android.view.View
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.littlebigpeople.itournamentmaker.viewholders.TournamentViewHolder
import org.hamcrest.Matcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


/**
 * Created by Dennis on 24-12-2017.
 */
open class NavigationTest {

    var authSignal: CountDownLatch? = null
    var firebaseUser: FirebaseUser? = null
    lateinit var auth: FirebaseAuth

    @Rule
    @JvmField
    public var activityTestRule = ActivityTestRule<MainActivity>(MainActivity::class.java)


    @Before
    fun init() {
        activityTestRule.activity.supportFragmentManager.beginTransaction()


        authSignal = CountDownLatch(1)
        auth = FirebaseAuth.getInstance()
        if (auth.getCurrentUser() == null) {
            auth.signInWithEmailAndPassword("test@schmock.eu", "obelix21").addOnCompleteListener(
                    object : OnCompleteListener<AuthResult> {
                        override fun onComplete(task: Task<AuthResult>) {
                            val result: AuthResult = task.getResult()
                            val user: FirebaseUser = result.getUser();
                            if (authSignal != null) {
                                (authSignal as CountDownLatch).countDown();
                            }
                        }
                    })
        } else {
            (authSignal as CountDownLatch).countDown();

        }
        (authSignal as CountDownLatch).await(10, TimeUnit.SECONDS)

    }


    @Test
    fun listTournamentsNavigationTest() {
        onView(ViewMatchers.withId(R.id.drawer_layout))
                .check(ViewAssertions.matches(DrawerMatchers.isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open())

        onView(ViewMatchers.withId(R.id.nvView))
                .perform(NavigationViewActions.navigateTo(R.id.nav_my_tournaments))


        onView(withId(R.id.rv_tournaments)).check(matches(isDisplayed()))

    }

    @Test
    fun showDashboardNavigationTest() {

        onView(ViewMatchers.withId(R.id.drawer_layout)).perform(DrawerActions.open())

        onView(ViewMatchers.withId(R.id.nvView))
                .perform((NavigationViewActions.navigateTo((R.id.nav_my_tournaments))))

        onView(withId(R.id.rv_tournaments)).perform(RecyclerViewActions.actionOnItemAtPosition<TournamentViewHolder>(0, MyViewAction.clickChildViewWithId(R.id.btn_tournament)))

        onView(withId(R.id.btn_players_list)).check(matches(isDisplayed()))

    }



    @Test
    fun navigatetoPlayersListTest() {

        onView(ViewMatchers.withId(R.id.drawer_layout)).perform(DrawerActions.open())

        onView(ViewMatchers.withId(R.id.nvView))
                .perform((NavigationViewActions.navigateTo((R.id.nav_my_tournaments))))

        onView(withId(R.id.rv_tournaments)).perform(RecyclerViewActions.actionOnItemAtPosition<TournamentViewHolder>(0, MyViewAction.clickChildViewWithId(R.id.btn_tournament)))

        onView(withId(R.id.btn_players_list)).perform(click())

        onView(withId(R.id.rv_players)).check(matches(isDisplayed()))
    }

//

    @Test
    fun navigateToDisciplineListTest() {

        onView(ViewMatchers.withId(R.id.drawer_layout)).perform(DrawerActions.open())

        onView(ViewMatchers.withId(R.id.nvView))
                .perform((NavigationViewActions.navigateTo((R.id.nav_my_tournaments))))

        onView(withId(R.id.rv_tournaments)).perform(RecyclerViewActions.actionOnItemAtPosition<TournamentViewHolder>(0, MyViewAction.clickChildViewWithId(R.id.btn_tournament)))

        onView(withId(R.id.btn_disciplines)).perform(click())

        onView(withId(R.id.rv_disciplines)).check(matches(isDisplayed()))
    }

    @Test
    fun navigateToMatchListTest() {

        onView(ViewMatchers.withId(R.id.drawer_layout)).perform(DrawerActions.open())

        onView(ViewMatchers.withId(R.id.nvView))
                .perform((NavigationViewActions.navigateTo((R.id.nav_my_tournaments))))

        onView(withId(R.id.rv_tournaments)).perform(RecyclerViewActions.actionOnItemAtPosition<TournamentViewHolder>(0, MyViewAction.clickChildViewWithId(R.id.btn_tournament)))

        onView(withId(R.id.btn_teams_list)).perform(click())

        onView(withId(R.id.rv_teams)).check(matches(isDisplayed()))
    }

    @Test
    fun teamListTest() {

        onView(ViewMatchers.withId(R.id.drawer_layout)).perform(DrawerActions.open())

        onView(ViewMatchers.withId(R.id.nvView))
                .perform((NavigationViewActions.navigateTo((R.id.nav_my_tournaments))))

        onView(withId(R.id.rv_tournaments))
                .perform(RecyclerViewActions.actionOnItemAtPosition<TournamentViewHolder>(0, MyViewAction.clickChildViewWithId(R.id.btn_tournament)))

        onView(withId(R.id.btn_teams_list)).perform(click())

        onView(withId(R.id.rv_teams)).check(matches(isDisplayed()))
    }

    @Test
    fun navigateScoreListTest() {

        onView(ViewMatchers.withId(R.id.drawer_layout)).perform(DrawerActions.open())

        onView(ViewMatchers.withId(R.id.nvView))
                .perform((NavigationViewActions.navigateTo((R.id.nav_my_tournaments))))

        onView(withId(R.id.rv_tournaments)).perform(RecyclerViewActions.actionOnItemAtPosition<TournamentViewHolder>(0, MyViewAction.clickChildViewWithId(R.id.btn_tournament)))

        onView(withId(R.id.btn_scoreboard)).perform(click())

        onView(withId(R.id.rv_scores)).check(matches(isDisplayed()))
    }
}

object MyViewAction {

    fun clickChildViewWithId(id: Int): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View>? {
                return null
            }

            override fun getDescription(): String {
                return "Click on a child view with specified id."
            }

            override fun perform(uiController: UiController, view: View) {
                val v = view.findViewById(id) as View
                v.performClick()
            }
        }
    }

}