package com.littlebigpeople.itournamentmaker

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.DrawerActions.open
import android.support.test.espresso.contrib.DrawerMatchers.isClosed
import android.support.test.espresso.contrib.NavigationViewActions.navigateTo
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import android.view.Gravity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


/**
 * Created by Dennis on 21-12-2017.
 */
open class AuthTest {

    var authSignal: CountDownLatch? = null
    var firebaseUser: FirebaseUser? = null
    lateinit var auth: FirebaseAuth

    @Rule
    @JvmField
    public var activityTestRule = ActivityTestRule<MainActivity>(MainActivity::class.java)


    @Before
    fun init() {
        activityTestRule.activity.supportFragmentManager.beginTransaction()


        authSignal = CountDownLatch(1)
        auth = FirebaseAuth.getInstance()
        if (auth.getCurrentUser() == null) {
            auth.signInWithEmailAndPassword("test@schmock.eu", "obelix21").addOnCompleteListener(
                    object : OnCompleteListener<AuthResult> {
                        override fun onComplete(task: Task<AuthResult>) {
                            val result: AuthResult = task.getResult()
                            val user: FirebaseUser = result.getUser();
                            if (authSignal != null) {
                                (authSignal as CountDownLatch).countDown();
                            }
                        }
                    })
        } else {
            (authSignal as CountDownLatch).countDown();

        }
        (authSignal as CountDownLatch).await(10, TimeUnit.SECONDS)

    }

    @Test
    fun testLogin() {
        if(auth!=null) {
            auth.signOut()

        }

        //pre-condition
        assertTrue(auth.currentUser==null)

        authSignal = CountDownLatch(1)
        auth = FirebaseAuth.getInstance()

        if (auth.getCurrentUser() == null) {
            auth.signInWithEmailAndPassword("test@schmock.eu", "obelix21").addOnCompleteListener(
                    object : OnCompleteListener<AuthResult> {
                        override fun onComplete(task: Task<AuthResult>) {
                            val result: AuthResult = task.getResult()
                            val user: FirebaseUser = result.getUser();
                            if (authSignal != null) {
                                (authSignal as CountDownLatch).countDown();
                            }
                        }
                    })
        } else {
            (authSignal as CountDownLatch).countDown();

        }
        (authSignal as CountDownLatch).await(10, TimeUnit.SECONDS)


        assertTrue(auth.currentUser != null)
        assertTrue("Expected test@schmock.eu, got: " + auth.currentUser?.email, auth.currentUser?.email.equals("test@schmock.eu"))
        assertFalse(auth.currentUser == null)

    }

    @Test
    fun testLogout() {

        //pre-condition - logged in
        assertTrue(auth.currentUser!=null)
        onView(withId(R.id.drawer_layout))
                .perform(open())

        onView(withId(R.id.nvView))
                .perform(navigateTo(R.id.nav_logout))

        //post-condition - logged out
        assertTrue(auth.currentUser == null)

    }


    @Test
    fun clickOnDrawerOpenShowDrawer() {
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.LEFT)))
                .perform(open())

        onView(withId(R.id.nvView))
                .perform(navigateTo(R.id.nav_join_tournament))

        val expectedText = InstrumentationRegistry.getTargetContext()
                .getString(R.string.join_tournament)
        onView(withId(R.id.textview_join)).check(matches(withText(expectedText)))
    }




}