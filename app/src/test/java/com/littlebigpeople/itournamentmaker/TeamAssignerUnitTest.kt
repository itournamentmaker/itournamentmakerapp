package com.littlebigpeople.itournamentmaker

import com.littlebigpeople.itournamentmaker.data.Player
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.util.TeamAssigner
import com.littlebigpeople.itournamentmaker.util.TeamAssignerImpl
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.*


class TeamAssignerUnitTest {
    @Test
    fun splitTeamsDefaultImplementation() {
        val teamAssigner : TeamAssigner = TeamAssignerImpl()
        val players  = ArrayList<Player>()
        players.add(Player("1","John","",""))
        players.add(Player("2","James","",""))
        players.add(Player("3","Janet","",""))
        players.add(Player("4","Jimmy","",""))
        players.add(Player("5","Jill","",""))
        players.add(Player("6","Julie","",""))
        players.add(Player("7","Job","",""))
        players.add(Player("8","Jenny","",""))

        val generateTeams : List<Team> = teamAssigner.generateTeams(players, 2, "Unique Tournament Id")
        assertTrue(generateTeams.size == 2)
        assertTrue(generateTeams[0].players.size == 4 && generateTeams[1].players.size == 4)

    }

    @Test
    fun threeTeamsDefaultImplementation() {
        val teamAssigner : TeamAssigner = TeamAssignerImpl()
        val players  = ArrayList<Player>()
        players.add(Player("1","John","",""))
        players.add(Player("2","James","",""))
        players.add(Player("3","Janet","",""))
        players.add(Player("4","Jimmy","",""))
        players.add(Player("5","Jill","",""))
        players.add(Player("6","Julie","",""))
        players.add(Player("7","Job","",""))
        players.add(Player("8","Jenny","",""))

        val generateTeams : List<Team> = teamAssigner.generateTeams(players, 3, "Unique Tournament Id")
        assertTrue(generateTeams.size == 3)
        assertTrue(generateTeams[0].players.size == 3 && generateTeams[1].players.size == 3 && generateTeams[2].players.size == 2)

    }
}
