package com.littlebigpeople.itournamentmaker

import com.littlebigpeople.itournamentmaker.data.Discipline
import com.littlebigpeople.itournamentmaker.data.Player
import com.littlebigpeople.itournamentmaker.data.Team
import com.littlebigpeople.itournamentmaker.data.TournamentDetails
import com.littlebigpeople.itournamentmaker.tournament.AllVsAll
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.*

class AllVsAllUnitTest {




    @Test
    public fun generateTournamentTest(){

        val players  = ArrayList<Player>()
        players.add(Player("1","John","",""))
        players.add(Player("2","James","",""))
        players.add(Player("3","Janet","",""))
        players.add(Player("4","Jimmy","",""))
        players.add(Player("5","Jill","",""))
        players.add(Player("6","Julie","",""))
        players.add(Player("7","Job","",""))
        players.add(Player("8","Jenny","",""))

        val teams = ArrayList<Team>()
        val teamA: Team = Team("A",players, "Team A",0,"UniqueId")
        val teamB: Team = Team("B",players, "Team B",0,"UniqueId")
        val teamC: Team = Team("C",players, "Team C",0,"UniqueId")

        teams.add(teamA)
        teams.add(teamB)
        teams.add(teamC)

        val disciplines: ArrayList<Discipline> = ArrayList()
        val discipline1: Discipline = Discipline("Bowling", "With balls and pins","1")
        val discipline2: Discipline = Discipline("Jogging", "With legs","2")
        val discipline3: Discipline = Discipline("Balloon Dancing", "With balloons and legs","3")

        disciplines.add(discipline1)
        disciplines.add(discipline2)
        disciplines.add(discipline3)

        val tournamentDetails: TournamentDetails = TournamentDetails("Tournament", "Bob")

        val allVsAll :AllVsAll = AllVsAll()

        val generatedTournament = allVsAll.generateTournament(tournamentDetails.user, tournamentDetails.name, 0,teams, disciplines, tournamentDetails)

        for (match in generatedTournament.matches){
            println(match.discipline.name)
            println(match.teamA.name + " " +match.teamB.name)
        }
        assertTrue("Expected: 9, got: "+generatedTournament.matches.size,generatedTournament.matches.size == 9)
    }
}
