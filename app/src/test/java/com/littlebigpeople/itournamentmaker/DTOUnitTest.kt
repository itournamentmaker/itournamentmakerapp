package com.littlebigpeople.itournamentmaker

import com.littlebigpeople.itournamentmaker.data.Player
import com.littlebigpeople.itournamentmaker.data.Team
import org.junit.Assert
import org.junit.Test

/**
 * Created by danie on 12/14/2017.
 */


class DTOUnitTest {
    @Test
    fun playerDTO_isCorrect() {
        val player = PlayerDTO("id","name","team","tuuid")
        Assert.assertEquals(player, Player(player).getPlayerDTO())
    }

    @Test
    fun teamDTO_isCorrect() {
        val team = TeamDTO("id","name",0,"UniqueId")
        Assert.assertEquals(team, Team(team).getTeamDTO())
    }


}